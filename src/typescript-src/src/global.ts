/// ----------------------------------------------------------
/// This file is used to declare global variables and functions.
/// ----------------------------------------------------------

/// !IMPORTANT: This file must export at least one variable or function to declare global variables and functions.

import { PaintballEvaluatorMsg } from './msg_definitions/paintball_evaluator_msg';
import { reset_robot_msg_list } from './msg_manager';
import { getButtonHandler } from './ui/button_handler';
import { getGameStats } from './ui/game_stats';
import { getStatusHandler } from './ui/status_handler';
import { Main } from './main';


declare global {
    interface Window {
        //**---------------------- Variables ----------------------**//
        GDO: any;                                   // Godot options - Initialized in index.html.
        Engine: any;                                // Godot engine class - Initialized in index.js.
        python_godot_manager: Main;                 // TODO: Remove this variable under window object.
        emi: PaintballEvaluatorMsg;                 // eval_msg_inter - Initialized in godot.
        rmli: [any];                                // robot_msg_list_inter - Initialized in godot.
        scenario_files: [any]

        //**---------------------- Functions ----------------------**//
        loadPyodide: Function;                      // Loads Pyodide - Initialized in index.html (Pyodide script import).

        //**--------- Button Used ---------**//
        shrink_expand_statusBar: Function;          // Shrinks/Expands status bar - Initialized in window.onload event.
        startUserScript: Function;                  // Starts user script - Initialized in window.onload event.
        clearTerminal: Function;                    // Clears terminal - Initialized in window.onload event.
        restartSimulationH: Function                // Calls "python_godot_manager"s restart simulation function - Initialized in window.onload event.
        // changeCamera: Function;                  => Under godot Callbacks.
        switchTerminal: Function;                   // Opens/Hides terminal - Initialized in window.onload event.
        // pauseSimulationH: Function;               => Under godot Callbacks.
        switchAutoScroll: Function;                 // Switches auto scroll - Initialized in window.onload event.
        // changeCamera: Function;                  => Under godot Callbacks.
        
        //**--------- Godot Callbacks ---------**//
        notifyGodot: Function;                      // Notify Godot
        restartSimulation: Function;                // Restarts simulation - Initialized in godot.
        startSimulation: Function;                  // Starts simulation - Initialized in godot.
        pauseSimulation: Function;                  // Pauses simulation - Initialized in window.onload event.
        changeCamera: Function;                     // Changes camera - Initialized in window.onload event.
        
        //**--------- Godot Used ---------**//
        sensor_msg_factory: Function;               //
        get_robot_msg: Function;                    //
        get_evaluator_msg: Function;                //
        setSimState: Function;                      //
        notifyPython: Function;  
        sendMapToApi: Function;                   //
        
    }
}

// This global function will be defined by Godot.
// The states are defined in GameManager.gd
export function setSimState(state: number) {
   
    switch(state) {
        case 0: // Initializing
            break;
        case 1: // Running
            getButtonHandler().setToRunning();
            getStatusHandler().setToRunning();
            break;
        case 2: // Ready
            getButtonHandler().setToReady();
            getStatusHandler().setToReady();
            break;
        case 3: // Paused
            getButtonHandler().setToPaused();
            getStatusHandler().setToPaused();
            break;
        case 4: // Restarting
            getStatusHandler().setToRestarting();
            getButtonHandler().setToRestarting();
            reset_robot_msg_list();
            break;
        case 5: // Finished
            getButtonHandler().setToFinished();
            getStatusHandler().setToFinished();
            getGameStats().updateUI();
            sendFinishedGameScoreToApi();
            break;
    } 
}

function sendFinishedGameScoreToApi() {
    const base_url: string = new URL(window.location.href).origin;

    const api_url: URL = new URL(`${base_url}/finished-game-stats/`);

    // Post eval_msg_inter as json string to the api
    fetch(api_url.toString(), { // TODO: api_url.toString() Might be a problem. IDK
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(window.emi)
    })
}

export function sendMapToApi() {
    const base_url: string = new URL(window.location.href).origin;

    const api_url: URL = new URL(`${base_url}/send-map-file/deneme`); // deneme should be in variable

    fetch(api_url.toString(), { // TODO: api_url.toString() Might be a problem. IDK
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(window.scenario_files)
    })
}