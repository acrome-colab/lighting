import {} from './global';
import { CameraSensorMsg } from './msg_definitions/camera_sensor_msg';
import { GpsSensorMsg } from "./msg_definitions/gps_sensor_msg";
import { ImuSensorMsg } from "./msg_definitions/imu_sensor_msg";
import { DistanceSensorMsg } from "./msg_definitions/distance_sensor_msg";
import { PaintballRobotMsg } from "./msg_definitions/paintball_robot_msg";


export function reset_robot_msg_list() {
    Object.assign(window.rmli, [], {length: 0});
}

export function sensor_msg_factory(sensor_node_type: string, parent_name: string) {
    let robot = get_robot_msg(parent_name);
    let sensor_msg;
    
    switch (sensor_node_type) {
        case "camera":
            sensor_msg = new CameraSensorMsg();
            robot.camera.push(sensor_msg);
            break
        case "gps":
            sensor_msg = new GpsSensorMsg();
            robot.gps.push(sensor_msg);
            break;
        case "imu":
            sensor_msg = new ImuSensorMsg();
            robot.imu.push(sensor_msg);
            break;
        case "distance":
            sensor_msg = new DistanceSensorMsg();
            robot.distance.push(sensor_msg);
            break;
        default:
            sensor_msg = new GpsSensorMsg();
            robot.gps.push(sensor_msg);
            break;
    }

    return sensor_msg;
}

// Gets robot message from robot_msg_list_inter
// Currently we only have 1 type of robot (paintball), so we do not check the robot type and create PaintballRobotMsg by default.
export function get_robot_msg(robot_name: string) {
    for (let robot of window.rmli) {
        if (robot.name == robot_name) {
            return robot;
        }
    }
    // If robot is not found, create a new robot message and add it to the list.
    let robot = new PaintballRobotMsg();
    robot.name = robot_name;
    window.rmli.push(robot);
    return robot;
}

// Currently we only have 1 type of evaluator (paintball), so we do not check the eval type and create PaintballEvalMsg by default.
export function get_evaluator_msg(eval_node_type: string) {
    return window.emi;
}

export function get_file_msg(){
    return window.scenario_files;
}
