import {} from '../global';

export class GodotManager {

    private engine: any;
    private readonly engineConfig: any;
    private readonly myWasm: string = 'index';
    private readonly myPck: string = 'index.pck';

    constructor() {
        /// ------------------------------------------------------
        this.loadGodotEngine = this.loadGodotEngine.bind(this);
        this.startGame = this.startGame.bind(this);
        this.startSimulation = this.startSimulation.bind(this);
        this.restartSimulation = this.restartSimulation.bind(this);
        this.pauseSimulation = this.pauseSimulation.bind(this);
        /// ------------------------------------------------------
        this.engineConfig = window.GDO;
    }

    // Loads Godot Engine and returns a promise that resolves when game is ready to be started.
    public async loadGodotEngine() {
        this.engine = new window.Engine(this.engineConfig);

        return Promise.all([
            // Load and init the engine
            this.engine.init(this.myWasm),
            // And the pck concurrently
            this.engine.preloadFile(this.myPck),
        ]);
    }

    public async startGame() {
        await this.engine.start({ args: ['--main-pack', this.myPck]});
    }

    //****************** Below Functions Are Godot Callbacks ******************//
    public startSimulation() {
        // TODO: TEMP.
        window.startSimulation();
    }
    public restartSimulation() {
        // TODO: TEMP.
        window.restartSimulation();
    }
    public pauseSimulation() {
        // TODO: TEMP.
        window.pauseSimulation();
    }
}
