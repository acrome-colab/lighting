
// Returns the URI of a file provided by the Fast Api. Currently used to fetch RidersLib Python Wheel and user_code.
export function getRidersLibDownloadUri(file_path: string) {
    let search_params = new URLSearchParams(window.location.search);
    let base_url = new URL(window.location.href).origin;

    if (search_params.has('ws')) { // If custom url is given
        base_url = search_params.get('ws');
    }

    return base_url.concat(file_path);
}

// Fetch user_script as string from api
export async function getUserScript(script_name: string) {
    let uri = getRidersLibDownloadUri(`/user-code/${script_name}`);

    let response = await fetch(uri).then(resp => resp.text());
    // Strip " from start and end of string
    response = response.substring(1, response.length - 1);
    // replace //n with /n
    response = response.replace(/\\n/g, "\n");
    return response;
}
