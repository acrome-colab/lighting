import {} from '../global';
import { PythonTerminal } from '../ui/python_terminal';
import { getRidersLibDownloadUri, getUserScript } from './pyodide_service';


export class PyodideManager {
    pythonTerminal: PythonTerminal;
    pyodide: any;

    constructor() {
        /// ------------------------------------------------------
        this.fetchAndLoadPyodide = this.fetchAndLoadPyodide.bind(this);
        this.startUserScript = this.startUserScript.bind(this);
        this.process = this.process.bind(this);
        this.restart = this.restart.bind(this);
        /// ------------------------------------------------------
        
        this.pythonTerminal = new PythonTerminal();
    }

    async fetchAndLoadPyodide() {
        this.pyodide = await window.loadPyodide({
            indexURL: "https://cdn.jsdelivr.net/pyodide/v0.19.0/full/",
            stdout: this.pythonTerminal.appendMessage,
            stderr: this.pythonTerminal.appendError
        });
        
        await Promise.all([
            this.pyodide,
            this.pyodide.loadPackage("micropip"),
        ]).then(async function(results) {
            let uri = getRidersLibDownloadUri("/python/riders_lib-1.0-py3-none-any.whl");
            await results[0].runPythonAsync(`
                import micropip
                await micropip.install("${uri}")
                from src import main
                lib = main.RiderLib()
            `);
        });
    }

    // TODO: make this dynamic
    // Returns False if an error occurred
    async startUserScript() {
        const user_script = await getUserScript("user_code.py");

        // Check syntax error
        try {
            this.pyodide.runPython(user_script);

            this.pyodide.runPython(`lib.initUserCode(UserCode, "ScoutRider")`); // TODO: Currently it is ScoutRider
            
        } catch (e) {
            this.pythonTerminal.appendError(e.message);
            return false;
        }
        return true;
    }

    // Returns False if an error occurred
    process() {
        try {
            this.pyodide.runPython(`
            lib.fetch_sensor_data()
            `);
        } catch (e) {
            this.pythonTerminal.appendError(e.message);
            return false;
        }
        return true;
    }

    restart() {
        this.pyodide.runPython(`lib.is_comm_managers_inited = False`);
    }
}
