import {} from '../global';


class GameStats {

    speed_element: HTMLElement;
    rotation_element: HTMLElement;
    time_element: HTMLElement;
    front_sensor_element: HTMLElement;
    back_sensor_element: HTMLElement;
    right_sensor_element: HTMLElement;
    left_sensor_element: HTMLElement;
    score_info_section: HTMLElement;
    camera_sensor_canvas_element: HTMLCanvasElement;
    footer: HTMLElement;
    robot_position_element: HTMLElement;
    score_element: HTMLElement;
    robot_score_elements: any;

    game_stat_msg: any;
    player_robot: any;

    constructor(game_stat_msg: any) {
        /// ------------------------------------------------------
        this.init_post_load_vars = this.init_post_load_vars.bind(this);
        this.restart = this.restart.bind(this);
        this.updateUI = this.updateUI.bind(this);
        this.updateSpeed = this.updateSpeed.bind(this);
        this.updateRotation = this.updateRotation.bind(this);
        this.updateScore = this.updateScore.bind(this);
        this.updateTime = this.updateTime.bind(this);
        this.updateFrontSensor = this.updateFrontSensor.bind(this);
        this.updateBackSensor = this.updateBackSensor.bind(this);
        this.updateRightSensor = this.updateRightSensor.bind(this);
        this.updateLeftSensor = this.updateLeftSensor.bind(this);
        this.updateRobotScores = this.updateRobotScores.bind(this);
        this.updateRobotPosition = this.updateRobotPosition.bind(this);
        this.init_robot_score_elements = this.init_robot_score_elements.bind(this);
        this.drawCameraSensorToCanvas = this.drawCameraSensorToCanvas.bind(this);
        this.shrink_expand_statusBar = this.shrink_expand_statusBar.bind(this);
        /// ------------------------------------------------------

        // this.speed_element = document.getElementById("speed");
        // this.rotation_element = document.getElementById("rotation");
        // this.time_element = document.getElementById("time");
        // this.front_sensor_element = document.getElementById("front_sensor");
        // this.back_sensor_element = document.getElementById("back_sensor");
        // this.right_sensor_element = document.getElementById("right_sensor");
        // this.left_sensor_element = document.getElementById("left_sensor");
        // this.score_info_section = document.getElementById("score_info_sec");
        this.camera_sensor_canvas_element = document.getElementById("camera_sensor_canvas") as HTMLCanvasElement;
        this.footer = document.getElementById("footer");
        // this.robot_position_element = document.getElementById(`robot_position`);
        // this.score_element = document.getElementById(`score`);

        this.robot_score_elements = {};
        this.game_stat_msg = game_stat_msg;
    }
    
    // Initialized variables dependent on godot.
    // Called after godot is initialized.
    init_post_load_vars() {
        this.init_robot_score_elements();

        // @ts-ignore
        this.player_robot = window.rmli[1];
    }

    restart() {
        return;
        this.speed_element.innerText = '0';
        this.rotation_element.innerText = '0';
        this.time_element.innerText = '0';

        this.front_sensor_element.innerText = '0';
        this.robot_position_element.innerText = '0, 0';
        this.score_element.innerText = '0';

        // Called below function to clear robot scores.
        // It will work cause GAMESTATS.restart() function is called after godot is restarted.
        // So robots' scores are already cleared.
        this.updateRobotScores();
    }

    updateUI() {
        return;
        // @ts-ignore
        this.player_robot = window.rmli[1];
        
        this.updateSpeed();
        this.updateRotation();
        this.updateFrontSensor();
        this.updateRobotScores();
        this.updateTime();
        this.updateRobotPosition();
        this.updateScore();

        // this.drawCameraSensorToCanvas();
    }

    updateSpeed() {
        return;
        const speed = this.player_robot.imu[0].speed;
        
        this.speed_element.innerText = speed.toFixed(2);
    }

    updateRotation() {
        return;
        const rotation = this.player_robot.imu[0].rotation;

        this.rotation_element.innerText = rotation.toFixed(2);
    }

    updateScore() {
        return;
        this.score_element.innerText = this.game_stat_msg.score.toFixed(2);
    }

    updateTime() {
        return;
        this.time_element.innerText = String(this.game_stat_msg.time_limit - this.game_stat_msg.current_time);
    }

    updateFrontSensor() {
        return;
        const front_sensor = this.player_robot.distance[0].distance;

        this.front_sensor_element.innerText = front_sensor.toFixed(2);
    }

    updateBackSensor() {
        // Currently unavailable
        return;
        const back_sensor = 0;

        this.back_sensor_element.innerText = back_sensor.toFixed(2);
    }

    updateRightSensor() {
        // Currently unavailable
        return;
        const right_sensor = 0;

        this.right_sensor_element.innerText = right_sensor.toFixed(2);
    }

    updateLeftSensor() {
        // Currently unavailable
        return;
        const left_sensor = 0;

        this.left_sensor_element.innerText = left_sensor.toFixed(2);
    }

    updateRobotScores() {
        return;
        // Etkin: Normally there should only be "for loop" and "this.robot_score_elements[robot_name].innerText = String(score);" line. inside.
        // But some reason, I could not find so, on user interface "F1Car"s score is not updated and I had to clear footer's score column and then create it again.
        // Score elements are not created again, they are only updated and put in the same place.
        this.score_info_section.innerHTML = ``;
        for (const [robot_name, score] of Object.entries(this.game_stat_msg.robot_scores)) {
            this.robot_score_elements[robot_name].innerText = `${robot_name}: ${String(score)}`;

            // this.score_info_section.innerHTML += `${robot_name}: `;
            // this.score_info_section.appendChild(this.robot_score_elements[robot_name]);
            // this.score_info_section.appendChild(document.createElement(`br`));
        }

        // Below two loops are used to order robot scores.
        for (const [robot_name, score] of Object.entries(this.game_stat_msg.robot_scores)) {
            if (robot_name.endsWith(`AI`)) continue;
            this.score_info_section.appendChild(this.robot_score_elements[robot_name]);
            this.score_info_section.appendChild(document.createElement(`br`));
        }

        for (const [robot_name, score] of Object.entries(this.game_stat_msg.robot_scores)) {
            if (!robot_name.endsWith(`AI`)) continue;
            this.score_info_section.appendChild(this.robot_score_elements[robot_name]);
            this.score_info_section.appendChild(document.createElement(`br`));
        }

    }

    updateRobotPosition() {
        return;
        this.robot_position_element.innerHTML = `${this.player_robot.gps[0].y.toFixed(2)}, ${this.player_robot.gps[0].x.toFixed(2)}`;
    }

    async init_robot_score_elements() {
        return;
        for (const [robot_name, score] of Object.entries(this.game_stat_msg.robot_scores)) {
            this.robot_score_elements[robot_name] = document.createElement(`span`);
            this.robot_score_elements[robot_name].id = `${robot_name}_score`;
            if (robot_name.endsWith(`AI`)) this.robot_score_elements[robot_name].style.color = `#0f0`;
            this.robot_score_elements[robot_name].innerHTML = `${robot_name}: 0`;

            // this.score_info_section.innerHTML += `${robot_name}: `;

            // this.score_info_section.appendChild(this.robot_score_elements[robot_name]);
            // this.score_info_section.appendChild(document.createElement(`br`));
        }
        // Below two loops are used to order robot scores
        for (const [robot_name, score] of Object.entries(this.game_stat_msg.robot_scores)) {
            if (robot_name.endsWith('AI')) continue;
            this.score_info_section.appendChild(this.robot_score_elements[robot_name]);
            this.score_info_section.appendChild(document.createElement(`br`));
        }

        for (const [robot_name, score] of Object.entries(this.game_stat_msg.robot_scores)) {
            if (!robot_name.endsWith('AI')) continue;
            this.score_info_section.appendChild(this.robot_score_elements[robot_name]);
            this.score_info_section.appendChild(document.createElement(`br`));
        }

    }
    
    // Draws the image in camera_msg to given canvas.
    async drawCameraSensorToCanvas() {
        return;
        const camera_msg = this.player_robot.camera[0];

        var rgbData = camera_msg.byte_arr;
        var width = camera_msg.width;
        var height = camera_msg.height;

        this.camera_sensor_canvas_element.width = width;
        this.camera_sensor_canvas_element.height = height;
        
        var mContext = this.camera_sensor_canvas_element.getContext('2d');
        var mImgData = mContext.createImageData(width, height);
        
        var srcIndex=0, dstIndex=0, curPixelNum=0;
        
        for (curPixelNum=0; curPixelNum<width*height;  curPixelNum++)
        {
            mImgData.data[dstIndex] = rgbData[srcIndex];		// r
            mImgData.data[dstIndex+1] = rgbData[srcIndex+1];	// g
            mImgData.data[dstIndex+2] = rgbData[srcIndex+2];	// b
            mImgData.data[dstIndex+3] = 255; // 255 = 0xFF - constant alpha, 100% opaque
            srcIndex += 3;
            dstIndex += 4;
        }
        mContext.putImageData(mImgData, 0, 0);
    }
    
    /**
     * Shrinks or expands status bar.
     */
    shrink_expand_statusBar() {
        this.footer.classList.toggle("shrink_status");
        this.footer.classList.toggle("expand_status");
    }

}

var GAMESTATS: GameStats;
export function getGameStats(): GameStats {
    if (!GAMESTATS) GAMESTATS = new GameStats(window.emi);
    return GAMESTATS;
}
