// import {} from '../global';

export class PythonTerminal {

    terminal: HTMLInputElement;
    terminal_area: HTMLElement;
    show_terminal_button: HTMLElement;
    scroll_msg: HTMLElement;

    canScroll: boolean = true;


    constructor() {
        /// ------------------------------------------------------
        this.appendMessage = this.appendMessage.bind(this);
        this.appendError = this.appendError.bind(this);
        this.switchAutoScroll = this.switchAutoScroll.bind(this);
        this.showError = this.showError.bind(this);
        this.showMessage = this.showMessage.bind(this);
        this.showAllOutputs = this.showAllOutputs.bind(this);
        this.switchTerminal = this.switchTerminal.bind(this);
        this.openTerminal = this.openTerminal.bind(this);
        this.clearTerminal = this.clearTerminal.bind(this);
        this.stackMessage = this.stackMessage.bind(this);
        this.scrollBottom = this.scrollBottom.bind(this);
        /// ------------------------------------------------------

        this.terminal = document.getElementById('terminal') as HTMLInputElement;
        this.terminal_area = document.getElementById('terminal_area');
        this.show_terminal_button = document.getElementById('show_terminal_button');
        this.scroll_msg = document.getElementById('auto_scroll_msg');
    }

    async appendMessage(message: string) {
        if (!message.endsWith('\n')) message += '\n';

        this.stackMessage(message);
        
        this.terminal.value += message;
        if (this.canScroll) this.scrollBottom();
    }

     async appendError(message: string) {
        if (!message.endsWith('\n')) message += '\n';

        this.stackMessage(message);

        this.terminal.value += message;
        if (this.canScroll) this.scrollBottom();
    }

    async switchAutoScroll() {
        this.canScroll = !this.canScroll;
        if (this.canScroll) {
            this.scroll_msg.style.color = 'green';
            this.scroll_msg.innerText = 'Otomatik Kaydırmayı Kapat';
        } else {
            this.scroll_msg.style.color = 'red';
            this.scroll_msg.innerText = 'Otomatik Kaydırmayı Aç';
        }
    }

    async showError() {
        // TODO (Etkin): show only errors
    }
    async showMessage() {
        // TODO (Etkin): show only messages
    }
    async showAllOutputs() {
        // TODO (Etkin): show all outputs
    }

    /**
     * Shows or hides terminal
     */
    async switchTerminal() {
        this.terminal_area.style.display = this.terminal_area.style.display == 'none' ? 'block' : 'none';
        this.show_terminal_button.innerText = this.terminal_area.style.display == 'none' ? 'Terminali Aç' : 'Terminali Kapat';
    }

    async openTerminal() {
        this.terminal_area.style.display = 'block';
        this.show_terminal_button.innerText = 'Terminali Kapat';
    }

    async clearTerminal() {
        this.terminal.value ='';
    }

    stackMessage(message: string) {
        if (this.terminal.value.endsWith(message)) {
            this.terminal.value = this.terminal.value.substring(0, this.terminal.value.lastIndexOf(message))
            if (this.terminal.value.endsWith('\n') || this.terminal.value == '')
                this.terminal.value += '(2) '
            else {
                let newStack = 1 + Number(this.terminal.value.substring(this.terminal.value.lastIndexOf('(') + 1, this.terminal.value.lastIndexOf(')')))
                this.terminal.value = this.terminal.value.substring(0, this.terminal.value.lastIndexOf('(')) + '(' + String(newStack) + ') ';
            }
        }
    }

    scrollBottom() {
        this.terminal.scrollTop = this.terminal.scrollHeight;
    }
}
