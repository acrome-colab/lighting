// import {} from '../global';

class TerminalResizeHandler {
    resizer: HTMLElement;
    terminal: HTMLElement;
    terminal_area: HTMLElement;
    cursor_x: number = 0;
    beginning_width: number;
    resizing: boolean = false;
    limits: any = {};

    constructor() {
        /// ------------------------------------------------------
        this.mouseDownHandler = this.mouseDownHandler.bind(this);
        this.mouseMoveHandler = this.mouseMoveHandler.bind(this);
        this.mouseUpHandler = this.mouseUpHandler.bind(this);
        /// ------------------------------------------------------
        
        this.limits.max_width = window.innerWidth - 10;
        this.limits.min_width = 250;

        this.resizer = document.getElementById('resizer');
        this.resizer.addEventListener('mousedown', this.mouseDownHandler.bind(this));

        document.addEventListener('mousemove', this.mouseMoveHandler.bind(this));
        document.addEventListener('mouseup', this.mouseUpHandler.bind(this));

        this.terminal_area = document.getElementById('terminal_area');
        this.terminal = document.getElementById('terminal');
    }

    mouseDownHandler(event: any) {
        if (this.resizing)
            return;
        // this.terminal.style.userSelect = 'none';
        this.limits.max_width = window.innerWidth - 10
        this.cursor_x = event.clientX;

        this.beginning_width = this.terminal_area.offsetWidth;

        this.terminal_area.style.userSelect = 'none';
        this.resizing = true;
    }

    mouseMoveHandler(event: any) {
        if (!this.resizing)
            return;
        let val = (this.beginning_width + (this.cursor_x - event.clientX))
        if (val <= this.limits.max_width && val >= this.limits.min_width) {
            this.terminal_area.style.width = `${val}px`;
        } else if (val > this.limits.max_width) {
            this.terminal_area.style.width = `${this.limits.max_width}px`;
        } else {
            this.terminal_area.style.width = `${this.limits.min_width}px`;
        }
    }

    mouseUpHandler(ignored: any) {
        if (!this.resizing) 
            return;

        this.resizing = false;
        this.terminal_area.style.userSelect = 'auto';
    }
    
}

var TERMINALRESIZEHANDLER: TerminalResizeHandler;
/**
 * Should be used as initalizer for TerminalResizeHandler.
 * @returns {TerminalResizeHandler}
 */
export function getTerminalResizeHandler(): TerminalResizeHandler {
    if (!TERMINALRESIZEHANDLER) TERMINALRESIZEHANDLER = new TerminalResizeHandler();
    return TERMINALRESIZEHANDLER;
}
