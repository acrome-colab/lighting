// import {} from '../global';

class ButtonHandler {

    private runButton: HTMLButtonElement;
    private restartButton: HTMLButtonElement;
    private gifElement: HTMLImageElement;
    private canvasElement: HTMLCanvasElement;
    private terminalButton: HTMLButtonElement;
    // private pauseButton: HTMLButtonElement;
    private changeCameraButton: HTMLButtonElement;

    constructor() {
        /// ------------------------------------------------------
        this.setToReady = this.setToReady.bind(this);
        this.setToRestarting = this.setToRestarting.bind(this);
        this.setToRunning = this.setToRunning.bind(this);
        this.setToPaused = this.setToPaused.bind(this);
        this.setToFinished = this.setToFinished.bind(this);
        /// ------------------------------------------------------
        
        this.runButton = document.getElementById('start_button') as HTMLButtonElement;
        this.restartButton = document.getElementById('restart_button') as HTMLButtonElement;
        this.gifElement = document.getElementById('gif') as HTMLImageElement;
        this.canvasElement = document.getElementById('canvas') as HTMLCanvasElement;
        this.terminalButton = document.getElementById('show_terminal_button') as HTMLButtonElement;
        // this.pauseButton = document.getElementById('pause_button');
        this.changeCameraButton = document.getElementById('change_camera_button') as HTMLButtonElement;
        
        this.runButton.disabled = true;
        this.runButton.style.display = 'inline-block';
        this.restartButton.disabled = true;
        this.restartButton.style.display = 'none';
        this.terminalButton.disabled = true;
        // this.pauseButton.disabled = true;
        this.changeCameraButton.disabled = true;
    }

    setToReady() {
        this.runButton.disabled = false;
        this.runButton.style.display = 'inline-block';
        this.restartButton.disabled = true;
        this.restartButton.style.display = 'none';
        this.terminalButton.disabled = false;
        // this._pauseButton.disabled = true;
        this.changeCameraButton.disabled = false;
        this.gifElement.style.display = 'none';
        this.canvasElement.style.display = 'inherit';
    }

    // TODO: This is not working. Not sure why.
    setToRestarting() {
        this.runButton.disabled = true;
        this.runButton.style.display = 'inline-block';
        this.restartButton.disabled = true;
        this.restartButton.style.display = 'none';
        // this.pauseButton.disabled = true;
        this.changeCameraButton.disabled = true;

        this.gifElement.style.display = 'inherit';
        this.canvasElement.style.display = 'none';
    }

    setToRunning() {
        this.runButton.disabled = true;
        this.runButton.style.display = 'none';
        this.restartButton.disabled = false;
        this.restartButton.style.display = 'inline-block';
        // this.pauseButton.disabled = false;
        this.changeCameraButton.disabled = false;
    }

    setToPaused() {
        // Currently unavailable.
        return;
        this.runButton.disabled = false;
        this.restartButton.disabled = false;
        // this.pauseButton.disabled = true;
        this.changeCameraButton.disabled = false;
    }

    setToFinished() {
        this.runButton.disabled = true;
        this.runButton.style.display = 'none';
        this.restartButton.disabled = false;
        this.restartButton.style.display = 'inline-block';
        // this.pauseButton.disabled = true;
    }
}

var BUTTONHANDLER: ButtonHandler;
export function getButtonHandler() {
    if (!BUTTONHANDLER) BUTTONHANDLER = new ButtonHandler();
    return BUTTONHANDLER;
}