import {} from '../global';


class StatusHandler {
    private messageField: HTMLElement;

    constructor() {
        /// ------------------------------------------------------
        this.setToReady = this.setToReady.bind(this);
        this.setToRestarting = this.setToRestarting.bind(this);
        this.setToRunning = this.setToRunning.bind(this);
        this.setToPaused = this.setToPaused.bind(this);
        this.setToFinished = this.setToFinished.bind(this);
        /// ------------------------------------------------------
        this.messageField = document.getElementById('message');
        this.messageField.innerText = 'Simülasyon Hazırlanıyor...';
    }

    setToReady() {
        this.messageField.innerText = 'Hazır!';
    }

    setToRestarting() {
        this.messageField.innerText = 'Yenileniyor...';
    }

    setToRunning() {
        this.messageField.innerText = 'Çalışıyor';
    }

    setToPaused() {
        // Currently unavailable.
        //this.messageField.innerText = 'Paused';
    }

    setToFinished() {
        let message = window.emi.status;

        message = String(message).replace('Zaman Doldu! ', 'Zaman Doldu!<br>');
        this.messageField.innerHTML = message;
    }
}

var STATUSHANDLER: StatusHandler;
export function getStatusHandler(): StatusHandler {
    if (!STATUSHANDLER) STATUSHANDLER = new StatusHandler();
    return STATUSHANDLER;
}
