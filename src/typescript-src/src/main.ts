import {} from './global';
import { PyodideManager } from './python/pyodide_manager';
import { GodotManager } from './godot/godot_manager';
import { getGameStats } from './ui/game_stats';
import { getButtonHandler } from './ui/button_handler';


// This is the entry point of the whole project.
// Everyting will be managed under this class
export class Main {

    pyodideManager: PyodideManager;
    godotManager: GodotManager;

    constructor() {
        /// ------------------------------------------------------
        this.loadGodotPython = this.loadGodotPython.bind(this);
        this.notifyPython = this.notifyPython.bind(this);
        this.restartSimulation = this.restartSimulation.bind(this);
        this.pauseSimulation = this.pauseSimulation.bind(this);
        this.startUserScript = this.startUserScript.bind(this);
        /// ------------------------------------------------------
        
        this.pyodideManager = new PyodideManager();
        this.godotManager = new GodotManager();

        this.loadGodotPython();
    }

    // Loads Godot and Python scripts and starts the simulation
    async loadGodotPython() {
        await Promise.all([
            this.pyodideManager.fetchAndLoadPyodide(),
            this.godotManager.loadGodotEngine(),
        ]);
        await this.godotManager.startGame();

        // Some logic in gamestats constructor is dependent on the godot engine being loaded.
        getGameStats().init_post_load_vars();
        
        getButtonHandler().setToReady();
    }

    // This function need to be moved into its own relevant class.
    // This is called from Godot at every frame to notify pyodide. So it can fetch game state to process it.
    async notifyPython() {
        const result = this.pyodideManager.process();
        
        if (!result) {
            this.pauseSimulation();
            this.pyodideManager.pythonTerminal.openTerminal();
            return;
        }
        window.notifyGodot();
        
        getGameStats().updateUI();
    }

    async restartSimulation() {
        // Restart Godot
        this.godotManager.restartSimulation();
        // Restart Python
        this.pyodideManager.restart();
        // Restart UI status
        getGameStats().restart();
    }

    async pauseSimulation() {
        // Currently unavailable.
        return;
        this.godotManager.pauseSimulation();
    }

    async startUserScript() {
        const result = await this.pyodideManager.startUserScript();
        if (!result) {
            this.pauseSimulation();
            this.pyodideManager.pythonTerminal.openTerminal();
            console.log('Failed to start user script');
            return;
        }
        this.godotManager.startSimulation();
    }
}
