// import {} from '../global';

export class VehicleCommandMsg {

    steering_force: number = 0;
    accel_force: number = 0.0;
    sleep_time: number = 0;
    fire: boolean = false;

    // TODO (Etkin): Write description here.
    move: number = 0;
    turn: number = 0;

    constructor() {
        /// ------------------------------------------------------
        this.get_steering_force = this.get_steering_force.bind(this);
        this.get_accel_force = this.get_accel_force.bind(this);
        /// ------------------------------------------------------
    }

    public get_steering_force() {
        return this.steering_force;
    }

    public get_accel_force() {
        return this.accel_force;
    }
}
