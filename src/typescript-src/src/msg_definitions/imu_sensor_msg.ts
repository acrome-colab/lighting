// import {} from '../global';

export class ImuSensorMsg {

  speed: number = 0;
  rotation: number = 0;
  name: string = '';

  constructor() {
    /// ------------------------------------------------------
    this.print = this.print.bind(this);
    /// ------------------------------------------------------
  }

  public print() {
    console.log("ImuSensorMsg: " + this.name + " " + this.speed + " " + this.rotation);
  }
}