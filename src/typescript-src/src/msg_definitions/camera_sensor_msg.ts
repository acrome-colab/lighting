// import {} from '../global';

export class CameraSensorMsg {

    width: number = 0;
    height: number = 0;
    byte_arr: [] = [];
    name: string = '';
    is_constants_set: boolean = false;
    byte_arr_size: number = 0;

    constructor() {
        /// ------------------------------------------------------
        this.init_constants = this.init_constants.bind(this);
        this.set_byte_arr = this.set_byte_arr.bind(this);
        this.print = this.print.bind(this);
        /// ------------------------------------------------------
    }

    public init_constants(width: number, height: number) {
        this.width = width;
        this.height = height;

        this.byte_arr_size = width * height * 3;

        this.is_constants_set = true;
    }

    // Decode hexadecimanl string into byte array
    public set_byte_arr(hex_str: string) {
        if (!this.is_constants_set) throw new Error("Constants not set");

        var tokens = hex_str.match(/../gi);
        const new_arr = tokens.map(t => parseInt(t, 16));
        
        Object.assign(this.byte_arr, new_arr, {length: this.byte_arr_size});
    }

    public print() {
        // Print first 10 elements of byte array
        console.log(this.byte_arr.slice(0, 10));
    }
}
