// import {} from '../global';

export class DistanceSensorMsg {

  public distance: number = 0;
  public name: string = '';

  constructor() {
    /// ------------------------------------------------------
    this.print = this.print.bind(this);
    /// ------------------------------------------------------
  }

  public print() {
    console.log("DistanceSensorMsg: " + this.name + " " + this.distance);
  }
}
