// import {} from '../global';

export class GpsSensorMsg {

    public x: number = 0;
    public y: number = 0;
    public z: number = 0;
    public name: string = '';

    constructor() {
        /// ------------------------------------------------------
        this.update_values = this.update_values.bind(this);
        this.print = this.print.bind(this);
        /// ------------------------------------------------------
    }

    public update_values(x: number, y: number, z: number) {
        this.x = z;
        this.y = x;
        this.z = y;
    }

    public print() {
        console.log("GpsSensorMsg: " + this.name + " " + this.x + " " + this.y + " " + this.z);
    }
}
