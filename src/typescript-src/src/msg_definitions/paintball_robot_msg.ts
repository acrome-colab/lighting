// import {} from '../global';
import { CameraSensorMsg } from './camera_sensor_msg';
import { DistanceSensorMsg } from './distance_sensor_msg';
import { GpsSensorMsg } from './gps_sensor_msg';
import { ImuSensorMsg } from './imu_sensor_msg';
import { VehicleCommandMsg } from './vehicle_cmd_msg';


export class PaintballRobotMsg {

    vehicle_cmd: VehicleCommandMsg;
    camera: CameraSensorMsg[] = [];
    gps: GpsSensorMsg[] = [];
    distance: DistanceSensorMsg[] = [];
    imu: ImuSensorMsg[] = [];
    name: string = '';

    constructor() {
        this.vehicle_cmd = new VehicleCommandMsg()
        
        this.camera = []; // Array of CameraSensorMsg's
        this.gps = []; // Array of GpsSensorMsg's
        this.distance = []; // Array of DistanceSensorMsg's
        this.imu = []; // Imu sensor data (position, rotation etc.) This is array for consistency with the other sensors.
        
        this.name = ""; // Name of the Robot defined in Godot Editor
    }
}
