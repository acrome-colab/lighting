// import {} from '../global';

// This message definition is specific to the paintball game.
// It is used to send the game stats to the UI and to update backend.
export class PaintballEvaluatorMsg {

    public score_to_win: number = 0;
    public robot_scores: any = {};
    public current_time: number = 0;
    public time_limit: number = 0;
    public is_valid: boolean = false;
    public is_finished: boolean = false;
    public status: string = '';
    public winner: string = '';
    public score: number = 0;

    constructor() {
        // The maximum score a robot can get.
        // If a robot gets this score, it will win the game if session is valid.
        this.score_to_win = 0;

        // The current score of each robot.
        // Keys are robot names and values are number of times it hit an opponent robot.
        this.robot_scores = {};

        // Seconds elapsed since the game started.
        this.current_time = 0;

        // The time limit of the game in seconds.
        // If the time limit is reached before the maximum score is reached, the session will be invalid.
        this.time_limit = 0;

        // Keeps the validity state of the current session.
        // The session can be invalid for several reasons.
        // ie. If user changes code while simulation is running, the sessio will be invalid.
        // Another example. time limit is reached before any of the robots reached the maximum score.
        this.is_valid = false;

        // Keeps the finishe state of the current session.
        // It will be true whenever time limit is reached or one of the robot has scored the maximum score.
        // The can be true independent of the validity state.
        this.is_finished = false;
        
        // After game is finished, this value will be appear in the UI.
        this.status = '';

        // Winner of the game.
        this.winner = '';

        // Player Score:
        this.score = 0;
    }
}
