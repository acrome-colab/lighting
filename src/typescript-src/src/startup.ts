/// ----------------------------------------------------------
/// This Startup Script is used to initialize the application.
/// ----------------------------------------------------------

import { setSimState, sendMapToApi } from './global';
import { Main } from './main';
import { get_evaluator_msg, get_robot_msg, sensor_msg_factory } from './msg_manager';
import { PaintballEvaluatorMsg } from './msg_definitions/paintball_evaluator_msg';
import { getTerminalResizeHandler } from './ui/terminal_resize_handler';
import { getGameStats } from './ui/game_stats';
import { getStatusHandler } from './ui/status_handler';
import { getButtonHandler } from './ui/button_handler';


window.onload = function () {
    // TODO
    // TODO (Ali S.): Bellow this line hurts my eyes. Will refactor it and move it to its own relevant class.
    window.rmli = <any>[];
    window.emi = new PaintballEvaluatorMsg();
    window.scenario_files = <any>[];

    /** UI ELEMENTS **/
    getStatusHandler();
    getTerminalResizeHandler();
    getGameStats();
    getButtonHandler();
    
    /** MAIN LOOP **/
    window.python_godot_manager = new Main();

    /**
     * Section below initialize global functions.
     */
    
    //** USED BY BUTTON **//
    window.restartSimulationH = window.python_godot_manager.restartSimulation;
    // window.pauseSimulationH = window.python_godot_manager.pauseSimulation;

    window.shrink_expand_statusBar = getGameStats().shrink_expand_statusBar;
    window.startUserScript = window.python_godot_manager.startUserScript;
    window.clearTerminal = window.python_godot_manager.pyodideManager.pythonTerminal.clearTerminal;
    window.switchTerminal = window.python_godot_manager.pyodideManager.pythonTerminal.switchTerminal;
    window.switchAutoScroll = window.python_godot_manager.pyodideManager.pythonTerminal.switchAutoScroll;

    //** USED BY GODOT **/
    window.sensor_msg_factory = sensor_msg_factory;
    window.get_robot_msg = get_robot_msg;
    window.get_evaluator_msg = get_evaluator_msg;
    window.setSimState = setSimState;
    window.notifyPython = window.python_godot_manager.notifyPython;
    window.sendMapToApi = sendMapToApi;
}
