"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleCommandMsg = void 0;
class VehicleCommandMsg {
    constructor() {
        this.steering_force = 0;
        this.accel_force = 0.0;
        this.sleep_time = 0;
        this.fire = false;
        this.move = 0;
        this.turn = 0;
        this.get_steering_force = this.get_steering_force.bind(this);
        this.get_accel_force = this.get_accel_force.bind(this);
    }
    get_steering_force() {
        return this.steering_force;
    }
    get_accel_force() {
        return this.accel_force;
    }
}
exports.VehicleCommandMsg = VehicleCommandMsg;
