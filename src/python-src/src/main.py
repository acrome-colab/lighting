from typing import Dict, Callable
from .rider_lib.communication_manager import CommunicationManager, RiderRobot
from js import rmli, emi

# To build the python wheel use the following command:
# python3 setup.py bdist_wheel

class RiderLib:
    def __init__(self):
        self.comm_managers: Dict[str, CommunicationManager] = {}
        self.user_codes: Dict[str, Callable] = {}

        self.is_comm_managers_inited = False

    def init_comm_managers(self):
        
        for robot_msg in rmli:
            self.comm_managers[robot_msg.name] = CommunicationManager(robot_msg, emi)
        
        # self._init_sensor_data() # TODO: Currently no need this.
        self.is_comm_managers_inited = True

    def initUserCode(self, user_code, robot_name: str):
        if not self.is_comm_managers_inited:
            self.init_comm_managers()

        robot: RiderRobot = self.comm_managers[robot_name].robot
        self.user_codes[robot_name] = user_code(robot)
   

    # TODO (Ali S.): Find a better way to do this. Robot names should be dynamic.
    def _init_sensor_data(self):
        self._expose_comm_manager_to_another("RexRobot", "RexRobotAI")
        self._expose_comm_manager_to_another("RexRobotAI", "RexRobot")

    def _expose_comm_manager_to_another(self, exposer_robot_name: str, exposee_robot_name: str):
        self.comm_managers[exposee_robot_name].exposed_properties = self.comm_managers[exposer_robot_name].exposed_properties

    # This is called from godot in every frame
    # # Notify user code about new sensor data so it can process it
    def fetch_sensor_data(self):
        for robot_name in self.comm_managers:
            self.user_codes[robot_name].process_frame()
