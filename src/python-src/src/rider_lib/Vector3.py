# Custom Vector3 implementation that decodes gps_data from godot

class Vector3:
    def __init__(self, gps_data=None, x=0, y=0, z=0):
        self._x = x
        self._y = y
        self._z = z

        self._gps_data = gps_data

    @property
    def x(self):
        if self._gps_data is None:
            return self._x
        return self._gps_data.x

    @property
    def y(self):
        if self._gps_data is None:
            return self._y
        return self._gps_data.y

    @property
    def z(self):
        if self._gps_data is None:
            return self._z
        return self._gps_data.z

    @property
    def length(self):
        return (self.x**2 + self.y**2 + self.z**2)**0.5
    
    # Subtract two vectors
    def __sub__(self, other):
        return Vector3(x=self.x - other.x, y=self.y - other.y, z=self.z - other.z)
    
    # Add two vectors
    def __add__(self, other):
        return Vector3(x=self.x + other.x, y=self.y + other.y, z=self.z + other.z)

    # Length of the vector
    def __len__(self):
        self.length

    # Normalize the vector
    def normalize(self):
        length = len(self)
        if length == 0:
            return Vector3(0, 0, 0)
        return Vector3(x=self.x / length, y=self.y / length, z=self.z / length)

    # Dot product of two vectors
    def dot(self, other):
        return self.x * other.x + self.y * other.y + self.z * other.z

    # Matrix multiplication
    def __matmul__(self, other):
        return Vector3(x=self.x * other.x, y=self.y * other.y, z=self.z * other.z)

    # Scalar multiplication
    def __mul__(self, other):
        return Vector3(x=self.x * other, y=self.y * other, z=self.z * other)

    # Scalar division
    def __truediv__(self, other):
        return Vector3(x=self.x / other, y=self.y / other, z=self.z / other)

    def __str__(self):
        return '(' + str(self.x) + ', ' + str(self.y) + ', ' + str(self.z) + ')'