from .rider_robot import RiderRobot
from .Vector3 import Vector3

# 
class CommunicationManager:
    def __init__(self, robot_msg_js, evaluator_msg_js):
        self.robot = RiderRobot(robot_msg_js.vehicle_cmd, robot_msg_js.imu[0], robot_msg_js.distance, evaluator_msg_js)

        self._init_camera_sensors(robot_msg_js.camera)
        self._init_gps_sensors(robot_msg_js.gps)

    # Returns properties that other robot can access
    @property
    def exposed_properties(self):
        return {
            'position': self.robot.position
        }


    def _init_camera_sensors(self, camera_sensors):
        # For now RiderRobot.py only supports one camera sensor
        for camera in camera_sensors:
            self.robot.camera_height = camera.height
            self.robot.camera_width = camera.width

            self.robot.camera_data = camera.byte_arr
            break

        if len(camera_sensors) > 1:
            raise Exception("RiderRobot.py currently only supports one camera")



    def _init_gps_sensors(self, gps_sensors):
        # For now RiderRobot.py only supports one gps sensor
        for gps in gps_sensors:
            self.robot.position = Vector3(gps_data=gps)
            break

        if len(gps_sensors) > 1:
            raise Exception("RiderRobot.py currently only supports one gps")
            
    # Set robot's exposed properties
    @exposed_properties.setter
    def exposed_properties(self, exposed_properties):
        for exposed_property in exposed_properties:
            {
                'position': self.robot.set_opponent_postition
            }.get(exposed_property, lambda _: print('Unknown exposed property: ' + exposed_property))(exposed_properties[exposed_property])
