from tokenize import String
from .Vector3 import Vector3
import math


class RiderRobot:

    def __init__(self, vehicle_cmd_js, imu_sensor_js, distance_sensor_js, evaluator_js):
        self._evaluator_js = evaluator_js
        self._vehicle_cmd_js = vehicle_cmd_js
        self._imu_sensor_js = imu_sensor_js

        self.camera_height: int = 2
        self.camera_width: int = 8

        self.camera_data = []
        self.distance_sensor_data = distance_sensor_js

        self.position: Vector3
        self.opponent_position: Vector3

    def move_forward(self):
        self._vehicle_cmd_js.move = 1
    
    def move_backward(self):
        self._vehicle_cmd_js.move = -1

    def turn_right(self):
        self._vehicle_cmd_js.turn = -1

    def turn_left(self):
        self._vehicle_cmd_js.turn = 1

    @property
    def rotation_speed(self) -> float:
        return self._vehicle_cmd_js.steering_force

    @rotation_speed.setter
    def rotation_speed(self, speed: float):
        speed = speed / (math.pi*2) # Normalize to -1 to 1

        if abs(speed) > 1:
            speed = min(max(speed, -1), 1)
        self._vehicle_cmd_js.steering_force = speed

    @property
    def rotation(self) -> float:
        return self._imu_sensor_js.rotation
    
    @rotation.setter
    def rotation(self, rotation: float):
        raise RuntimeError('You cannot set rotation. Set rotation_speed instead.')
    
    @property
    def speed(self) -> float:
        return self._vehicle_cmd_js.accel_force

    # Sets the value of the speed of the robot
    @speed.setter
    def speed(self, speed: float):
        if abs(speed) > 1:
            speed = min(max(speed, -1), 1)
        self._vehicle_cmd_js.accel_force = speed

    @property
    def sleep_time(self) -> float:
        return self._vehicle_cmd_js.sleep_time

    @sleep_time.setter
    def sleep_time(self, sleep_time: float):
        self._vehicle_cmd_js.sleep_time = sleep_time

    # Returns time elapsed since the start of the game session
    @property
    def current_time(self) -> float:
        return self._evaluator_js.current_time

    def fire(self):
        if self._vehicle_cmd_js.fire:
            raise RuntimeError('Already shooting. Check if robot can shoot first. You can use robot.can_fire()')
        self._vehicle_cmd_js.fire = True
    
    # TODO (Ali S): ask godot if robot is ready to shoot
    def can_fire(self) -> bool:
        return not self._vehicle_cmd_js.fire

    @property
    # Returns the meaning of life
    def meaning_of_life(self) -> String:
        return '42'

    @meaning_of_life.setter
    def meaning_of_life(self, value: String):
        raise RuntimeError('You cannot set the meaning of life')

    # TODO: Can be done better
    # Set's opponent_position property for other robots to access this robots position
    def set_opponent_postition(self, position: Vector3):
        self.opponent_position = position
