extends StaticBody

#onready var shader = $lamp_base.mesh.surface_get_material(0).next_pass#$Pole.material.next_pass
var hovered = null

func _ready():
	GameManager.connect("send_selected_item", self, "_on_item_sended")

func _on_item_sended(item):
	pass
	#print(item)
#	if item == self:
#		shader.set_shader_param("grow",0.05)
#		hovered = true
#	else:
#		hovered = false
#		shader.set_shader_param("grow",0.0)

func get_cast():
	var camera = get_tree().get_root().get_node(GameManager.camera_node_path)
	var mouse = get_viewport().get_mouse_position()
	var ray_offset = 1.0
	var ray_length = 1000.0
	var ray_from = camera.project_ray_origin(mouse)
	var ray_to = ray_from + camera.project_ray_normal(mouse) * ray_length
	var ray = camera.get_world().direct_space_state.intersect_ray(ray_from, ray_to)
	return ray

func write_strings():
	return str(global_rotation.y)
	
func read_strings(str_array):
	rotate(Vector3(0,1,0), float(str_array[0]))
	#rotation_degrees.y = float(str_array[0])

func _on_Lamp_input_event(camera, event, position, normal, shape_idx):
	pass
	
	# Disabling drag for now - until I get other click issues worked out
	# will do this afterwards
	
	#if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
	#	if event.is_pressed():
	#		if hovered:
	#			GameManager.dragging = true
	#			set_physics_process_internal(true)
	#		else:
	#			GameManager.dragging = false

func _physics_process(delta):
	pass


func _on_Lamp_mouse_exited():
	hovered = false
