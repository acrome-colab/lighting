extends Spatial

onready var shader = $CSGBox2.material.next_pass#$Plane010.mesh.surface_get_material(1).next_pass#

func _ready():
	GameManager.connect("send_selected_item", self, "_on_item_sended")

func _on_item_sended(item):
	if item == self:
		shader.set_shader_param("grow",0.05)
	else:
		shader.set_shader_param("grow",0.0)

func write_strings():
	return ""
