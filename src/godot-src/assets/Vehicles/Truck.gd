extends KinematicBody

var current_road_list = null
var last_road = null
var next_road = null

var truck_speed = 1.0
var distance_connect_road = 0.02 # at this distance truck start to look for next road piece
var angle_turning_rate = 5.0
var lane_offset = 0.2 # how far over to shift to be in our lane

onready var shader = $CSGBox.material.next_pass

func _on_item_sended(item):
	if item == self:
		shader.set_shader_param("grow",0.05)
	else:
		shader.set_shader_param("grow",0.0)
		
# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.connect("send_selected_item", self, "_on_item_sended")

func write_strings():
	return ""

# GameManager will update the trucks anytime the road list changes
func update_roads(roads):
	last_road = null
	next_road = null
	current_road_list = roads

func get_closest_road():
	var best_dist_squared = 0
	var best_road = null
	if current_road_list != null:
		for road in current_road_list:
			var delta = road.transform.origin - transform.origin
			delta.y = 0
			var dist_squared = delta.length_squared()
			if best_road == null or dist_squared < best_dist_squared:
				best_dist_squared = dist_squared
				best_road = road
	return best_road

func get_linked_roads(seed_road):
	var links = []
	for road in current_road_list:
		var delta = road.transform.origin - seed_road.transform.origin
		var dx = int(stepify(delta.x, 1.0))
		var dz = int(stepify(delta.z, 1.0))
		var horizontal_link = dz == 0 and abs(dx) == 1
		var vertical_link = dx == 0 and abs(dz) == 1
		if horizontal_link or vertical_link:
			links.append(road)
	return links
	
func pick_next_road():
	var links = get_linked_roads(next_road)
	for link in links:
		if link != last_road:
			last_road = next_road
			next_road = link
			return
	
	# we could not find a new link which is not last_road so return last_road
	# now we will turn around
	var save_last_road = last_road
	last_road = next_road
	next_road = save_last_road
		
func _physics_process(delta: float):
	if GameManager.is_paused():
		return
	
	if next_road == null:
		next_road = get_closest_road()
	if next_road != null:
		var target = Vector3(next_road.transform.origin)

		# drive on the right side of the road
		if last_road != null:
			var road_segment_delta = (next_road.transform.origin - last_road.transform.origin).normalized()
			var side_vector = road_segment_delta.cross(Vector3(0,1,0))
			target += side_vector * lane_offset
			target -= road_segment_delta * lane_offset
		
		var delta_pos = target - transform.origin
		delta_pos.y = 0
		var velocity = delta_pos.normalized() * truck_speed

		var target_angle = atan2(velocity.x, velocity.z)
		
		var delta_angle = target_angle - deg2rad(rotation_degrees.y)
		while delta_angle > PI:
			delta_angle -= 2.0 * PI
		while delta_angle < -PI:
			delta_angle += 2.0 * PI
			
		rotate_y(delta_angle * delta * angle_turning_rate)

		# this modification makes the turning a bit more realistic
		var scale_speed = 1.0 - 5.0 * abs(delta_angle)/PI
		if scale_speed < 0.4:
			scale_speed = 0.4
		
		move_and_collide(velocity * delta * scale_speed)
		if delta_pos.length_squared() < distance_connect_road * distance_connect_road:
			pick_next_road()
		
		
