extends Spatial

var sensor_value = 0.0
#onready var shader = $Plane019.mesh.surface_get_material(1).next_pass#material.next_pass
#
#func _on_item_sended(item):
#	if item == self:
#		shader.set_shader_param("grow",0.05)
#	else:
#		shader.set_shader_param("grow",0.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.connect("send_selected_item", self, "_on_item_sended")

func set_lamp_list(lamps, letter):
	sensor_value = 0
	for lamp in lamps:
		# Temporary arbitrary calculation - need to work out the lighting model
		var lamp_position = lamp.transform.origin + lamp.transform.basis.x * 1.5
		var delta = lamp_position - transform.origin
		sensor_value += 1.0 / delta.length_squared()
	print("sensor value: ", sensor_value)
	var found = false
	for dict in GameManager.sensor_values:
		if self == dict['sensor']:
			found = true
			dict["letter"] = letter
			dict["value"] = sensor_value
	if not found:
		GameManager.sensor_values.append({"sensor":self,"letter":letter, "value":sensor_value})
	$HealthBar3D.update(sensor_value, letter)

func set_range(min_sensor_value, max_sensor_value):
	$HealthBar3D.set_range(min_sensor_value,max_sensor_value)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func write_strings():
	return ""
