extends Spatial

var angle = 0
var turn_rate = 180.0

func _ready():
	pass # Replace with function body.

func _process(delta):
	angle += turn_rate * delta
	
	rotation_degrees.y = angle
	angle = rotation_degrees.y

func write_strings():
	return ""
