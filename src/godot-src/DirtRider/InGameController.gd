extends Spatial

export(bool) var developer_mode = false
export(String) var auto_load = "auto_load_test"
export(int) var rotation_per_input = 30

var ui_list = [
	"TreeButton", "RoadButton", "TruckButton", "ScenarioMenu", "Save", "FileName",
	"MapSizeMenu", "EditorButton", "Building1Button", "Building2Button", "Building3Button"]
	
onready var treeButtonPosition = $TreeButton.rect_global_position
onready var scenario_menu_popup = $VBoxContainer/HBoxContainer3/ScenarioMenu.get_popup()
onready var sensor_range = $VBoxContainer
onready var sensor_value_min = $VBoxContainer/HBoxContainer/min_text
onready var sensor_value_max = $VBoxContainer/HBoxContainer2/max_text
onready var hide_hbox = $VBoxContainer/HBoxContainer3
onready var sensor_value_pop = $Popup
onready var sensor_values_box = $Popup/ColorRect/ScrollContainer/VBoxContainer
onready var selected_label = $SelectedItem
onready var http_request = $HTTPRequest
onready var file_saved_label = $Label
onready var timer = $Timer

var sended_item = null
var initiated = false
# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.auto_load_file_name = auto_load
	GameManager.set_developer_mode(developer_mode)
	GameManager.min_sensor_value = float(sensor_value_min.text)
	GameManager.max_sensor_value = float(sensor_value_max.text)
	GameManager.connect("update_sensor_values",self,"_on_update_sensors")
	GameManager.connect("send_selected_item",self,"_on_item_sended")
	GameManager.connect("item_placed",self, "_on_item_placed")
	GameManager.update_sensors()
	if not developer_mode:
		# set delete button position to near of the sensor button
		$DeleteButton.rect_global_position = treeButtonPosition
		# set scenario for auto load
	
	# This will actually load LampTest.gd after initial.gd which is not what we wanted I think
		#for i in range(scenario_menu_popup.get_item_count()):
	#		if scenario_menu_popup.get_item_text(i) == auto_load:
	#			GameManager.menu_selected(i)
		# hide items for not developers
		for i in get_children():
			if i.name in ui_list:
				i.hide()
		hide_hbox.hide()
	else:
		pass
		
	if OS.get_name() == "HTML5":
		get_map_with_http_request(auto_load)
		#send_map_with_http_request(auto_load)
	else:
		#get_map_with_http_request(auto_load)
		GameManager.open_scenario_as_text_file(auto_load)
	initiated = true

func _input(event):
	if Input.is_action_just_pressed("rotate_right") and sended_item != null:
		sended_item.rotation_degrees.y = stepify(sended_item.rotation_degrees.y - rotation_per_input, rotation_per_input)
		GameManager.update_sensors()
		GameManager.emit_signal("item_placed")
	elif Input.is_action_just_pressed("rotate_left") and sended_item != null:
		sended_item.rotation_degrees.y = stepify(sended_item.rotation_degrees.y + rotation_per_input, rotation_per_input)
		GameManager.update_sensors()
		GameManager.emit_signal("item_placed")
	elif Input.is_action_just_pressed("ui_cancel"):
		sended_item = null
	
	# I was implementing delete from Tool button so might be messing something up here
	# will consider later	
	#elif Input.is_action_just_pressed("delete_item"):
	#	GameManager.delete_item(sended_item)

func send_map_with_http_request(file_name):
	var file = File.new()
	file.open(GameManager.scenario_file_path + file_name + ".gd", File.READ)
	var content = file.get_as_text()
	var body = to_json({"name": file_name, "content":content, "developerMode":developer_mode})
	var error = http_request.request(GameManager.send_map_file_url, [], true, HTTPClient.METHOD_POST, body)
	if error != OK:
		print(error)
		print("error while send map file")
	file.close()
	return error

func get_map_with_http_request(file_name):
	var error = http_request.request(GameManager.get_map_file_url+file_name)
	if error != OK:
		print(error)

# send_selected_item signal handler
func _on_item_sended(item):
	sended_item = item
	print(sended_item)
	if "Sensor" in sended_item.name:
		var letter = GameManager.set_letter(sended_item)
		selected_label.text = "Selected item: Sensor_" + letter
	elif "Spatial" in sended_item.name:
		selected_label.text = "Selected item: Road"
	elif "Static" in sended_item.name:
		selected_label.text = "Selected item: Building"
	elif "tree" in sended_item.name:
		selected_label.text = "Selected item: Tree"
	elif "Lamp" in sended_item.name:
		selected_label.text = "Selected item: Lamp"
	elif "Kinematic" in sended_item.name:
		selected_label.text = "Selected item: Truck"


# update sensor signal handler
func _on_update_sensors(sensor,letter):
	for child in sensor_values_box.get_children():
		sensor_values_box.remove_child(child)
		child.queue_free()
		
	for sensor_value in GameManager.sensor_values:
		var label = Label.new()
		label.text = sensor_value['letter'] + ": " + str(sensor_value['value'])
		sensor_values_box.add_child(label)

func _on_min_text_text_changed():
	GameManager.min_sensor_value = float(sensor_value_min.text)
	GameManager.update_sensors()

func _on_max_text_text_changed():
	GameManager.max_sensor_value = float(sensor_value_max.text)
	GameManager.update_sensors()
	
# sensor values button clicked 
func _on_Button_pressed():
	if not sensor_value_pop.visible and len(GameManager.sensors) > 0:
		sensor_value_pop.show()
	else:
		sensor_value_pop.hide()


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var response = parse_json(body.get_string_from_utf8())
	print(response)
	if response.has("items"):
		for a in GameManager.all_lists:
			GameManager.clear_list(a)
		var item_array = response["items"]
		for item in item_array:
			var name = item[0]
			if name == "MapSize":
				GameManager.world_size = item[1]
			else:
				var x = int(item[1])
				var y = int(item[2])
				var placed_item = GameManager.place_item(name, x, y)
				if name == "Lamp":
					placed_item.read_strings([item[3]])
		GameManager.finalize_level_setup()

#file save handler
#this works when dragging finished, item rotated, item placed and file saved
func _on_item_placed():
	if initiated:
		if OS.get_name() == "HTML5":
			var error = send_map_with_http_request(auto_load)
			if error == OK:
				file_saved_label.show()
				timer.start()
		else:
			GameManager.save_scenario_as_text_file(auto_load)
			file_saved_label.show()
			timer.start()


func _on_Timer_timeout():
	file_saved_label.hide()
