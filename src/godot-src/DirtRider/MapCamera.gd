extends Camera

export(float, 0.0, 1.0) var sensitivity = 0.4

var interp_camera_origin : Vector3
var interp_camera_yaw  = 0.0
var is_first_interp = true

var dragging = false
var control_down = false
var shift_down = false
var rotate_state = false
var pan_state = false

var linear_momentum = Vector3(0,0,0)
var minimum_linear_momentum = 0.001 # linear units
var linear_momentum_response_rate = 0.001
var linear_momentum_decay_rate = 8.0

var angular_momentum = Vector3(0,0,0)
var minimum_angular_momentum = 0.1 # degrees units
var angular_momentum_response_rate = 0.06
var angular_momentum_decay_rate = 8.0

var max_angular_momentum = 10.0

var min_zoom = 160
var max_zoom = 10
var step_zoom = 3

# for orbital camera
var bOrbitalCamera = true # lock on object so we can pan around it
var orbital_distance = 4.0
var min_orbital_distance = 0.1
var max_orbital_distance = 10.0
var orbital_target = Vector3(0,0,0)
var orbital_target_interpolate_target = Vector3(0,0,0) # orbital_target interpolates to this
var max_orbital_target_interpolate_alpha = 0.01
var orbital_target_interpolate_alpha = max_orbital_target_interpolate_alpha # how fast we interpolate

# Mouse state
var _mouse_position = Vector2(0.0, 0.0)
var _total_pitch = 0.0
var _previousPosition: Vector2 = Vector2(0, 0);
var position = Vector2(0.0, 0.0)

func event_new_sceneario_loaded():
	var w = GameManager.world_size * GameManager.world_cell_size

	orbital_target = Vector3(w/2.0, 0, w/2.0)
	orbital_target_interpolate_target = orbital_target

	orbital_distance = GameManager.world_size * 0.8

func event_set_new_orbital_focus(focus, window_click_percent):
	return # disable this feature for now

	orbital_target_interpolate_target = focus

	# squared function - very slow center 
	var factor = window_click_percent * window_click_percent
	orbital_target_interpolate_alpha = max_orbital_target_interpolate_alpha * factor

	# never let orbital_target_interpolate_target go outside the map size
	var w = GameManager.world_size * GameManager.world_cell_size
	orbital_target_interpolate_target.x = clamp(orbital_target_interpolate_target.x, 0, w)
	orbital_target_interpolate_target.z = clamp(orbital_target_interpolate_target.z, 0, w)

	# always stay on the map surface for now
	orbital_target_interpolate_target.y = 0



# Called when the node enters the scene tree for the first time.
func _ready():
	interp_camera_origin = transform.origin
	interp_camera_yaw = rotation_degrees.y

func _input(event):
	if event is InputEventKey:
		if event.scancode == KEY_CONTROL:
			if event.pressed:
				control_down = true
			else:
				control_down = false
		if event.scancode == KEY_SHIFT:
			if event.pressed:
				shift_down = true
			else:
				shift_down = false
				
	if event is InputEventMouseButton:
#		if event.button_index == BUTTON_LEFT:
#			if event.pressed:
#				pan_state = true
#			else:
#				pan_state = false
		if event.button_index == BUTTON_LEFT and control_down:
			if event.pressed:
				pan_state = true
			else:
				pan_state = false
		else:
			pan_state = false
			
		if event.button_index == BUTTON_LEFT and shift_down:
			if event.pressed:
				rotate_state = true
			else:
				rotate_state = false
		else:
			rotate_state = false
#		elif event.button_index == BUTTON_RIGHT:
#			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED if event.pressed else Input.MOUSE_MODE_VISIBLE)
			
		if event.button_index == BUTTON_WHEEL_DOWN:
			fov += step_zoom
			fov = clamp(fov, max_zoom, min_zoom)
		elif event.button_index == BUTTON_WHEEL_UP:
			fov -= step_zoom
			fov = clamp(fov, max_zoom, min_zoom)
			
	elif event is InputEventMouseMotion:
		_mouse_position = event.relative
		

#	if event is InputEventMouseMotion:
#		if control_down:
#			linear_momentum.z += event.relative.y * linear_momentum_response_rate
#		elif shift_down:
#			linear_momentum.x -= event.relative.x * linear_momentum_response_rate
#			linear_momentum.y += event.relative.y * linear_momentum_response_rate
#		elif dragging: # some hacky logic so we don't compete with GameManager tool dragging
#			angular_momentum.x += event.relative.y * angular_momentum_response_rate
#			angular_momentum.y += event.relative.x * angular_momentum_response_rate

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta: float):
	if pan_state:
		transform.origin.z -= _mouse_position.y * 0.03
		transform.origin.x -= _mouse_position.x * 0.03
	# Only rotates mouse if the mouse is captured
	if rotate_state:#Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		_mouse_position *= sensitivity
		var yaw = _mouse_position.x
		var pitch = _mouse_position.y
		_mouse_position = Vector2(0, 0)
		
		# Prevents looking up/down too far
		pitch = clamp(pitch, -90 - _total_pitch, 90 - _total_pitch)
		_total_pitch += pitch
	
		rotate_y(deg2rad(-yaw))
		rotate_object_local(Vector3(1,0,0), deg2rad(-pitch))
		
#	angular_momentum.x = clamp(angular_momentum.x, -max_angular_momentum, max_angular_momentum)
#	angular_momentum.y = clamp(angular_momentum.y, -max_angular_momentum, max_angular_momentum)
#	angular_momentum.z = clamp(angular_momentum.z, -max_angular_momentum, max_angular_momentum)
#
#	# rotation of camera
#	rotation_degrees.x += angular_momentum.x
#	rotation_degrees.y += angular_momentum.y
#
#	if not dragging:
#		if rotation_degrees.length() < minimum_angular_momentum:
#			rotation_degrees = Vector3(0,0,0)	
#
#	var angular_momentum_decay = delta * angular_momentum_decay_rate
#	if angular_momentum_decay > 1.0:
#		angular_momentum_decay = 1.0
#	angular_momentum -= angular_momentum * angular_momentum_decay
#
#	# translation of camera
#	transform.origin += transform.basis.x * linear_momentum.x
#	transform.origin += transform.basis.y * linear_momentum.y
#	transform.origin += transform.basis.z * linear_momentum.z
#
#	orbital_distance += linear_momentum.z
#	orbital_distance = clamp(orbital_distance, min_orbital_distance, max_orbital_distance)

#	var linear_momentum_decay = delta * linear_momentum_decay_rate
#	if linear_momentum_decay > 1.0:
#		linear_momentum_decay = 1.0
#	linear_momentum -= linear_momentum * linear_momentum_decay
#
#	if bOrbitalCamera:
#		var target_center = orbital_target
		#transform.origin = target_center + transform.basis.z * orbital_distance
#
#		if rotation_degrees.x < -80:
#			rotation_degrees.x = -80
#		if rotation_degrees.x > -10:
#			rotation_degrees.x = -10

#		orbital_target += (orbital_target_interpolate_target - orbital_target) * orbital_target_interpolate_alpha

func _on_EditorButton_pressed():
	pass # Replace with function body.
