#extends "res://addons/sensors/base_sensor.gd"
#
#onready var camera = $Viewport/Camera
#
#func _ready():
#	pass
#
## At each frame, move and rotate the Camera inside sensor to where the parent is.
#func _process(delta):	
#	if not GameManager.is_running():
#		return
#
#	var rootM = get_parent().global_transform
#
#	camera.global_transform.origin = rootM.origin
#
#	# use DebuggingViewport to determine these best values
#	camera.global_transform.origin += rootM.basis.y * 0.06
#	camera.global_transform.origin += rootM.basis.x * 0.16
#	camera.rotation_degrees.y = get_parent().rotation_degrees.y - 90
#
#	var im = camera.get_viewport().get_texture().get_data()
#	im.convert(Image.FORMAT_RGB8)
#	var all_data = im.get_data()
#
#	# read first row
#	var sx = camera.get_viewport().size.x
#	var sy = camera.get_viewport().size.y
#
#	var single_row_data = all_data.subarray(3 * sx * sy / 2, 3 * sx * sy / 2 + 3 * sx - 1)
#	var brightness_data = []
#	for i in range(single_row_data.size()/3):
#		var r = single_row_data[i*3+0]
#		var g = single_row_data[i*3+1]
#		var b = single_row_data[i*3+2]
#		var brightness = stepify((r+g+b)/(3.0 * 255.0), 0.001)
#		brightness_data.append(brightness)
#
#	GameManager.test_provide_camera_data(brightness_data)
#
#func get_node_type():
#	return "rider_sensor_camera"
