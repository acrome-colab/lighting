extends "res://addons/sensors/base_sensor.gd"

onready var camera = $camera

func _ready():
	pass

# At each frame, move and rotate the Camera inside sensor to where the parent is.
func _process(delta):	
	if not GameManager.is_running():
		return
	
#	camera.global_transform.origin = global_transform.origin
#	camera.rotation_degrees = get_parent().rotation_degrees + rotation_degrees

	var im = camera.get_viewport().get_texture().get_data()
	im.convert(Image.FORMAT_RGB8)
	# read first row
	var data = im.get_data().subarray(0, 3 * camera.get_viewport().size.x - 1)
	var brightness_data = []
	for i in range(data.size()/3):
		var r = data[i*3+0]
		var g = data[i*3+1]
		var b = data[i*3+2]
		var brightness = stepify((r+g+b)/(3.0 * 255.0), 0.01)
		brightness_data.append(brightness)
	
	# need to clean this up - decide how to best pass the data to ScoutRider.gd
	GameManager.test_provide_camera_data(brightness_data)
	
func get_node_type():
	return "rider_sensor_camera"
