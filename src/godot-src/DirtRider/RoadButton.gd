extends ToolButton

var tool_id = GameManager.ToolSelected.Road

func _ready():
	pass


func _on_RoadButton_button_down():
	if  GameManager.selected_tool == null:
		GameManager.tool_selected(tool_id)
	#var style_box = get_stylebox("Pressed","tool_box")
	

func _input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		GameManager.tool_selected(null)
		focus_mode = Control.FOCUS_NONE
		focus_mode = Control.FOCUS_CLICK


func _on_RoadButton_focus_entered():
	rect_scale.x = 1.25
	rect_scale.y = 1.25


func _on_RoadButton_focus_exited():
	GameManager.tool_selected(null)
	rect_scale.x = 1
	rect_scale.y = 1
