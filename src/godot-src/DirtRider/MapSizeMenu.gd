extends MenuButton

var step_until_size = 16 # then we start doubling
var num_sizes = 20 # total menu entries
var mutiplier = 2

func get_menu_value(id):
	var setting = 1
	for i in range(id):
		if setting < step_until_size:
			setting += 1
		else:
			setting *= mutiplier
	return setting
		
func _ready():
	var popup = get_popup()
	popup.connect("id_pressed", self, "file_menu")

	for i in range(num_sizes):
		var x = get_menu_value(i)
		popup.add_item(str(x) + "x" + str(x))

func file_menu(id):
	GameManager.set_world_size(get_menu_value(id))
		
	
