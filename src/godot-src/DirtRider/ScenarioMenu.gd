extends MenuButton

# Called when the node enters the scene tree for the first time.
func _ready():
	get_popup().connect("id_pressed", self, "file_menu")

func file_menu( id ):
	#print(id.name)
	# TODO can we do by name?
	GameManager.menu_selected(id)
