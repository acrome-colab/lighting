extends ConfirmationDialog

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _on_ConfirmationSave_confirmed():
	GameManager.save_file_confirmed()

func _on_ConfirmationSave_popup_hide():
	get_tree().paused = false
