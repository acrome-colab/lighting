extends Camera

export(float, 0.0, 1.0) var sensitivity = 0.25

# Mouse state
var _mouse_position = Vector2(0.0, 0.0)
var _total_pitch = 0.0

# Movement state
var _direction = Vector3(0.0, 0.0, 0.0)
var _velocity = Vector3(0.0, 0.0, 0.0)
var _acceleration = 30
var _deceleration = -10
var _vel_multiplier = 4

# Keyboard state
var _forward = false
var _backward = false
var _left = false
var _right = false
var _down = false
var _up = false
var _shift = false

var min_fov = 30
var max_fov = 110
var fov_step = 3

func _ready():
	pass

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			print("pressed")
			
		elif event.button_index == BUTTON_WHEEL_DOWN:
			self.fov += fov_step
			self.fov = clamp(self.fov, min_fov, max_fov)
			self.set_fov(fov)
			print(fov)
		elif event.button_index == BUTTON_WHEEL_UP:
			self.fov -= fov_step
			self.fov = clamp(self.fov, min_fov, max_fov)
			self.set_fov(fov)
			print(fov)
