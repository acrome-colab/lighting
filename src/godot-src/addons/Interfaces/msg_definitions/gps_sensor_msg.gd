class_name GpsSensorMsg

class _GpsSensorMsg:
	var x: float = 0
	var y: float = 0
	var z: float = 0
	var name: String = ''
	
	func update_values(x: float, y: float, z: float):
		self.x = x
		self.y = y
		self.z = z

