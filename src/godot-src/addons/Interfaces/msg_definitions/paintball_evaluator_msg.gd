class_name PaintballEvaluatorMsg

class _PaintballEvaluatorMsg:
	var score_to_win = 0
	var robot_scores = {}
	var current_time = 0
	var time_limit = 0
	var is_valid = false
	var is_finished = false
	var status = ''
	var winner = ''
	var score = 0

	func _to_string():
		return 'PaintballEvaluatorMsg(score_to_win=' + str(self.score_to_win) \
		+ ', robot_scores=' + str(self.robot_scores) \
		+ ', current_time=' + str(self.current_time) \
		+ ', time_limit=' + str(self.time_limit) \
		+ ', is_valid=' + str(self.is_valid) \
		+ ', is_finished=' + str(self.is_finished) \
		+ ', status=' + str(self.status) \
		+ ', winner=' + str(self.winner) \
		+ ', score=' + str(self.score) \
		+ ')'
