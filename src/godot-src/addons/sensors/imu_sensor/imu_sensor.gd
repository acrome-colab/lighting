extends "res://addons/sensors/base_sensor.gd"
	
var pi: float = 3.14159265
# Updates sensor_js to sensor's last state when CommunicationManager signals it
func _update_proto():
	sensor_js.speed = get_parent().current_speed / (get_parent().engine_power * GameConstant.delta_time)
	var rotation = get_parent().rotation.y + (pi/2)
	if rotation > pi * 2:
		rotation -= pi*2
		
	sensor_js.rotation = rotation

func get_node_type():
	return "rider_sensor_imu"
