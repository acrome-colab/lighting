extends "res://addons/sensors/base_sensor.gd"

onready var camera = $Viewport/camera

export(int, 1, 32) var viewport_resolution_x
export(int, 1, 32) var viewport_resolution_y

func _ready():
	# Sets viewport resolution
	assert(viewport_resolution_x >= viewport_resolution_y, "Viewport resolution's y size can not be greater than x. This feature is currently disabled.")
	var max_res = max(2, max(viewport_resolution_x, viewport_resolution_y))
	camera.get_viewport().size = Vector2(max_res, max_res)

	sensor_js.init_constants(viewport_resolution_x, viewport_resolution_y)

# At each frame, move and rotate the Camera inside sensor to where the parent is.
func _process(delta):
	if not GameManager.is_running():
		return
	camera.global_transform.origin = global_transform.origin
	camera.rotation_degrees = get_parent().rotation_degrees + rotation_degrees

func _get_feed_as_bytes():
	var im = camera.get_viewport().get_texture().get_data()
	im.convert(Image.FORMAT_RGB8)
	return im.get_data().subarray(0, 3 * viewport_resolution_x * viewport_resolution_y - 1)

# Updates sensor_js to sensor's last state when CommunicationManager signals it
func _update_proto():
	sensor_js.set_byte_arr(_get_feed_as_bytes().hex_encode())

func get_node_type():
	return "rider_sensor_camera"
