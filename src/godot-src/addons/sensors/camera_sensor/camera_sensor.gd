extends "res://addons/sensors/base_sensor.gd"

onready var camera = $Viewport/camera

export(int, 2, 32) var viewport_resolution_x
export(int, 2, 32) var viewport_resolution_y

func _ready():
	# Sets viewport resolution
	camera.get_viewport().size = Vector2(viewport_resolution_x, viewport_resolution_y)

	sensor_js.init_constants(viewport_resolution_x, viewport_resolution_y)

		
# At each frame, move and rotate the Camera inside sensor to where the parent is.
func _process(delta):
	if not CommunicationManager.is_running():
		return
	camera.global_transform.origin = global_transform.origin
	camera.rotation_degrees = get_parent().rotation_degrees + rotation_degrees

func _get_feed_as_bytes():
	var im = camera.get_viewport().get_texture().get_data()
	im.convert(Image.FORMAT_RGB8)
	return im.get_data()

# Updates sensor_js to sensor's last state when CommunicationManager signals it
func _update_proto():
	sensor_js.set_byte_arr(_get_feed_as_bytes().hex_encode())
	#sensor_js.set_byte_arr(String(_get_feed_as_bytes()))

func get_sensor_type():
	return "camera_sensor"
