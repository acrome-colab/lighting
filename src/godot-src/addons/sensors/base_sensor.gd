# Abstract Base for other sensor's to extend from
extends Spatial

var sensor_js: JavaScriptObject

func _ready():
	if !GameConstant.is_web_active:
		return
	sensor_js = CommunicationManager.get_js_msg(get_path())
	sensor_js.name = get_parent().name + "_" + name
	
	# Connect _update_proto function to update_sensors signal so that CommunicationManager can 
	# call _update_proto for sensor to update sensor_js
	GameManager.connect("update_sensors", self, "_update_proto")

# Updates sensor_js to sensor's last state when CommunicationManager signals it
func _update_proto():
	pass

# Returns the type of the sensor (camera_sensor, light_sensor, etc.)
func get_node_type():
	pass
