extends "res://addons/sensors/base_sensor.gd"
	
# Updates sensor_js to sensor's last state when CommunicationManager signals it
func _update_proto():
	var position_vec = global_transform.origin
	
	sensor_js.update_values(position_vec.x, position_vec.y, position_vec.z)

func get_node_type():
	return "rider_sensor_gps"
