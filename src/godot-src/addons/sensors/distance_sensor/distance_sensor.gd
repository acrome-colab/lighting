extends "res://addons/sensors/base_sensor.gd"

export(int, 50, 1000) var ray_length = 500

# Updates sensor_js to sensor's last state when CommunicationManager signals it
func _update_proto():
	var space_state = get_world().direct_space_state
	var ray = space_state.intersect_ray(global_transform.origin, global_transform.basis.x * ray_length, [self, get_parent()])
	if ray:
		var distance = global_transform.origin.distance_to(ray.position)
		sensor_js.distance = distance


func get_node_type():
	return "rider_sensor_distance"

