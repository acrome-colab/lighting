extends Node

# Usage:
# Add this node at aynwhere in the scene. (Just under the main node is recomended)
# Important: The 0 indexed camera will be the initial one.

export (Array, NodePath) var cameras = []
var current_index: int

func _ready():
	if cameras.size() > 0:
		# Sets old camera as initial, if game is restarted.
		if GameManager.camera_index != null:
			current_index = GameManager.camera_index
		else:
			current_index = 0
		
		for camera in cameras:
			get_node(camera).current = false
		
		get_node(cameras[current_index]).current = true
	UIManager.set_cameras(get_node(get_path()))
	GameManager.set_cameras(get_path())

# Next camera
func switch_camera():
	if cameras.size() <= 1:
		return
	var old = current_index
	current_index = (current_index + 1) if cameras.size() > (current_index + 1) else 0
	get_node(cameras[old]).current = false
	get_node(cameras[current_index]).current = true

# Previous camera
func previous_camera():
	if cameras.size() <= 1:
		return
	var old = current_index
	current_index = (current_index - 1) if (current_index - 1) >= 0 else (cameras.size() - 1)
	get_node(cameras[old]).current = false
	get_node(cameras[current_index]).current = true
