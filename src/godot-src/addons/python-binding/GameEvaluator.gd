extends Node

# Msg object defined in javascript as paintball_game_stats_msg.js
var game_stats_msg_js

var elapsed_time: float

signal finish_game

func _ready():
	game_stats_msg_js = CommunicationManager.get_js_msg(get_path())
	GameManager.connect("change_in_sim_state", self, "_on_change_in_sim_state")
	_reset_game_stats_values()
			
func _on_change_in_sim_state(state):
	if state == 2: # If state becomes to ready
		_reset_game_stats_values()

func _reset_game_stats_values():
	elapsed_time = 0
	
	game_stats_msg_js.score_to_win = 10
	game_stats_msg_js.time_limit = 120
	game_stats_msg_js.is_valid = true
	game_stats_msg_js.is_finished = false
	game_stats_msg_js.current_time = 0
	game_stats_msg_js.status = ""
	game_stats_msg_js.winner = ""
	game_stats_msg_js.score = 0
	
	var robot_nodes = get_tree().get_nodes_in_group("RiderRobot")
	for robot in robot_nodes:
		game_stats_msg_js.robot_scores[robot.name] = 0

func get_node_type():
	return "rider_evaluator_paintball"

func _physics_process(delta):
	if not GameManager.is_running():
		return
	update_elapsed_time()
	update_score()

# Updates elapsed time
func update_elapsed_time():
	elapsed_time += GameConstant.delta_time
	game_stats_msg_js.current_time = int(elapsed_time)

# Updates player score (Paintball)
func update_score():
	
	return # SKIP THIS FOR NOW
	
	if elapsed_time == 0:
		game_stats_msg_js.score = 0
	var time_percent = stepify(elapsed_time, 0.001) / game_stats_msg_js.time_limit
	# we lost some points as time increased - up to 50% of total score
	var scale_down_100 = 0.01
	var player_score: int
	var ai_score: int
	for robot in get_tree().get_nodes_in_group("RiderRobot"):
		if (String(robot.name).ends_with('AI')):
			ai_score = game_stats_msg_js.robot_scores[robot.name]
		else:
			player_score = game_stats_msg_js.robot_scores[robot.name]
	game_stats_msg_js.score = ((game_stats_msg_js.score_to_win + player_score - ai_score) * 10) * (1.0 - 0.5 * time_percent) * scale_down_100

# Called ball, when a it is shot from a robot and it hits to another robot.
# Updates the scores of the robots
func eval_ball_hit(sender_robot_name: String, receiver_robot_name: String):
	if game_stats_msg_js.is_finished:
		return
	
	game_stats_msg_js.robot_scores[sender_robot_name] = game_stats_msg_js.robot_scores[sender_robot_name] + 1

	if game_stats_msg_js.robot_scores[sender_robot_name] >= game_stats_msg_js.score_to_win:
		#_reset_game_stats_values()
		_on_finish()

# Called when the game is finished to set satus finished, determine the winner, send signal.
func _on_finish():
	game_stats_msg_js.is_finished = true
	# index 0: status
	# index 1: winner
	var result = ['', '']

	if elapsed_time > game_stats_msg_js.time_limit:
		_on_timeout(result)
	else:
		for robot in get_tree().get_nodes_in_group("RiderRobot"):
			if game_stats_msg_js.robot_scores[robot.name] == game_stats_msg_js.score_to_win:
				result[0] = robot.name + ' Kazandı!'
				result[1] = robot.name
				break
	
	game_stats_msg_js.status = result[0]
	game_stats_msg_js.winner = result[1]
	
	if !GameConstant.is_web_active:
		print(game_stats_msg_js)

	emit_signal("finish_game")

# In time out state, function calculates who won.
func _on_timeout(result: Array):
	var ai_score: int
	var player_score: int
	for robot in get_tree().get_nodes_in_group("RiderRobot"):
		if (String(robot.name).ends_with('AI')):
			ai_score = game_stats_msg_js.robot_scores[robot.name]
		else:
			player_score = game_stats_msg_js.robot_scores[robot.name]
	
	if (ai_score == player_score):
		result[0] = 'Zaman Doldu! Berabere!'
		result[1] = 'Tied'
	elif (ai_score > player_score):
		result[0] = 'Zaman Doldu! RexRobotAI Kazandı!'
		result[1] = 'RexRobotAI'
	else:
		result[0] = 'Zaman Doldu! RexRobot Kazandı!'
		result[1] = 'RexRobot'


