# This script is only responsible for communication between javascript and godot.
# It has nothing to do with counting time, calculating score,
# any type of game statistic and shooting groups.
# Do not put any game logic here.
extends Node

var _notify_godot_callback: JavaScriptObject
var _restart_simulation_callback: JavaScriptObject
var _start_simulation_callback: JavaScriptObject
var _pause_simulation_callback: JavaScriptObject
var _change_camera_callback: JavaScriptObject

var window_js: JavaScriptObject

func _ready():
	GameManager.connect("change_in_sim_state", self, "_on_change_in_sim_state")
	GameManager.connect("godot_is_ready_for_processing", self, "_on_godot_is_ready_for_processing")
	
	if GameConstant.is_web_active:
		window_js = JavaScript.get_interface('window')
		_notify_godot_callback = JavaScript.create_callback(self, "_notify_godot_callback")
		_restart_simulation_callback = JavaScript.create_callback(self, '_restart_simulation_callback')
		_start_simulation_callback = JavaScript.create_callback(self, "_start_simulation_callback")
		_pause_simulation_callback = JavaScript.create_callback(self, "_pause_simulation_callback")
		_change_camera_callback = JavaScript.create_callback(self, "_change_camera")
	else:
		return
	
	# Sets global functions to change the state of simulation
	window_js.restartSimulation = _restart_simulation_callback
	window_js.startSimulation = _start_simulation_callback
	# Pause functionality is deactivated for this version.
	# To activate it, uncomment the following line and remove "return" from JavaScript functions.
	# window_js.pauseSimulation = _pause_simulation_callback
	
	# Sets a global function in JS for Python to notify Godot that there is new
	# data to process.
	window_js.notifyGodot = _notify_godot_callback

	# Sets UI callbacks
	window_js.changeCamera = _change_camera_callback
	

# Called from each sensor that exist in main scene. So later CommunicationManager can get data from them to be
# processed by Python side.
# @returns: msg object that is initialized by and defined in javascipt.
func get_js_msg(sensor_node_path: NodePath):
	var rider_node: Node = get_node(sensor_node_path)
	var node_type: PoolStringArray = rider_node.get_node_type().split("_")

	if node_type[0] != "rider":
		push_error("Node type you are trying to append to CommunicationManager is not a rider node.")
		return

	match node_type[1]:
		"sensor":
			if !GameConstant.is_web_active:
				return
			return window_js.sensor_msg_factory(node_type[2], rider_node.get_parent().name)
		"robot":
			# Unlike other msg getter functions, this functions does not create
			#  a robot msg instance. The reason is sensor noded load first and
			#  they create the robot msg if non is created before. This msg getter
			#  function only returns the msg already created.
			if !GameConstant.is_web_active:
				return PaintballRobotMsg._PaintballRobotMsg.new()
			return window_js.get_robot_msg(rider_node.name)
		"evaluator":
			if !GameConstant.is_web_active:
				return PaintballEvaluatorMsg._PaintballEvaluatorMsg.new()
			return window_js.get_evaluator_msg(node_type[2])

func _on_godot_is_ready_for_processing():
	if !GameConstant.is_web_active:
		return
	window_js.notifyPython()

func _on_change_in_sim_state(sim_state: int):
	if !GameConstant.is_web_active:
		return
	window_js.setSimState(sim_state)

# Called from javascript to notify godot that it has processed user script.
func _notify_godot_callback(args):
	GameManager.on_godot_notified_by_javascript()

# Called from javascript to retart simlation
func _restart_simulation_callback(args):
	GameManager.restart()
	
# Called from javascript to start simlation
func _start_simulation_callback(args):
	GameManager.start()
	
# Called from javascript to pause simlation
func _pause_simulation_callback(args):
	GameManager.pause()

# Called from javascript to change camera view
func _change_camera(args):
	UIManager.change_camera()
