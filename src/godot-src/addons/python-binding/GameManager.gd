extends Node

const _update_python_frequency = 2

# This enum is also defined in js so before changing it make sure to change it there too.
# Currently it is defined in godot_manager.js
enum SimulationState {INITIALIZING, RUNNING, READY, PAUSED, RESTARTING, ENDED}
enum ToolSelected { # these are used to identify the Tool buttons for the editor
	Tree, Building1, Building2, Building3, Road, Sensor, Truck, Lamp, Delete
}

signal godot_is_ready_for_processing
signal vehicle_cmd_msg_js_change
signal update_sensors
signal change_in_sim_state
signal update_sensor_values
signal send_selected_item
signal item_placed

# lists of the setings and objects for the level
var world_size = 10 # map is NxN grid
var buildings1 = []
var buildings2 = []
var buildings3 = []
var trees = []
var squares = []
var roads = []
var sensors = []
var trucks = []
var lamps = []

var sensor_values = []

# things that do not snap to the grid
var no_grid_snap_lists = [sensors, lamps]

# things we can delete on
var delete_lists = [buildings1, buildings2, buildings3, trees,roads, sensors, trucks, lamps]

# things we can select on
# note this can be replaced by select_lists_playing below for dev mode off
var select_lists = [buildings1, buildings2, buildings3, trees, roads, sensors, trucks, lamps]

# use this list when not in developer mode
var select_lists_playing = [lamps, sensors]

# keep this synchronized with select_lists above
# these are the names written to the file
var select_list_names = ["Building1", "Building2", "Building3", "Tree", "Road", "Sensor", "Truck", "Lamp"]

var all_lists = [buildings1, buildings2, buildings3, trees, squares, roads, sensors, trucks, lamps]

# for some ordering issues between camera and game manager
# still need to work this all out with better design
var delay_send_camera_event = 0 # don't update camera for this many ticks - lets button event take priority
var set_orbital_camera_target = Vector3(0,0,0) # new target we will send
var set_orbital_camera_target_percent = 0.0 # the percent we will send

# settings for levels
var world_cell_size = 1.0 # currently the map is based on a 1 unit grid size

# editor variables
var editor_active = false # if editor is active the game stops and buttons appear at top for making changes
var waiting_to_save_file_name = "" # if we save file and it needs replace confirmation dialog this saves the plan - design seems wrong so still working out how godot shuld handle a confirmation dialog flow - we should not need this

var selected_tool = null # This will be ToolSelected above

# all the nodes which need to show or hide when editor toggles
var tool_node_names = [
	"TreeButton", "Building1Button", "Building2Button", "Building3Button",
	"RoadButton", "ScenarioMenu", "Save", "FileName", "MapSizeMenu", 
	"SensorButton", "DeleteButton", "TruckButton", "LampButton"]

var vbox_tool_node_name = ["ScenarioMenu", "Save", "FileName", "MapSizeMenu"]

var scenario_file_path = "res://ScenarioFiles//"
var scenario_file_path_export = "user://logs//ScenarioFiles//"

var _simulation_state: int # SimulationState

var min_sensor_value = 0
var max_sensor_value = 1.2

var _update_python_frequency_counter = 0

# for selecting items
var selected_halo = null # stores the selection object - which creates a halo light on the objet
var selected_item = null # the item we are focused on 
var _camera_manager: NodePath
var camera_index: int
var camera_node_path = "DirtRiderScene/MapCamera"

var loaded_file_name = ""
var get_map_file_url = ""
var send_map_file_url = ""

var rng = RandomNumberGenerator.new() # used for random numbers

var dragging_item = null
var dragging_item_offset = Vector3(0,0,0) # when we first click we store the relative offset to keep smooth interface while dragging and avoids a pop
var developer_mode = false
var auto_load_file_name = "LampTest"
var list_for_save_file
var dragging_state = false

func set_developer_mode(dev_mode):
	developer_mode = dev_mode
	list_for_save_file = select_lists
	if not dev_mode:
		# there is a bug in save file because of this
		
		select_lists = select_lists_playing
		
########################  SELECTION FUNCTİONS ########################
func menu_selected(id):
	var names = list_names_in_popup()
	print("Menu selected:", names[id])
	open_scenario_as_text_file(names[id])

func delete_pressed():
	if selected_item != null:
		delete_item(selected_item)
			
func tool_selected(tool_clicked):
	cancel_pending_camera_moves()
	selected_tool = tool_clicked

# convert the tool bar button id to a string name for methods like place_item
func tool_to_name(tool_id):
	return ToolSelected.keys()[tool_id]

func select_item(item, mouse_position = null):
	if item == null:
		# remove the select object
		if selected_halo:
			selected_halo.queue_free()
			selected_halo = null
		selected_item = null
	else:
		# make the selection object if needed
		#if selected_halo == null:
		#	selected_halo = load("res://assets/Select/Select.tscn").instance()
		#	call_deferred("add_child", selected_halo)
		selected_item = item
		dragging_item = item
		
		# we need an offset 
		var intercept = get_ray_to_ground_plane(mouse_position)
		dragging_item_offset = dragging_item.transform.origin - intercept 
		dragging_item_offset.y = 0
		
		#selected_halo.transform.origin = item.transform.origin
		#selected_halo.transform.origin.y += world_cell_size * 0.5
		#if selected_item.shader != null:
			#selected_item.shader.set_shader_param("grow",0.05)
		# temporary
		#if is_item_in_list(item, lamps):
			#item.rotation_degrees.y = stepify(item.rotation_degrees.y + 30.0 * input_rotation, 30.0)
			#update_sensors()
		emit_signal("send_selected_item",selected_item)

func delete_selection():
	selected_item = null

func get_ray(mouse_position):
	var camera = get_tree().get_root().get_node(camera_node_path)
	var ray_offset = 1.0
	var ray_length = 1000.0
	var ray_from = camera.project_ray_origin(mouse_position)
	var ray_to = ray_from + camera.project_ray_normal(mouse_position) * ray_length
	var ray = camera.get_world().direct_space_state.intersect_ray(ray_from, ray_to)
	return ray

func get_ray_to_ground_plane(mouse_position):
	var plane = Plane( Vector3(0,1,0), 0)
	var camera = get_tree().get_root().get_node(camera_node_path)
	var ray_from = camera.project_ray_origin(mouse_position)
	var ray_length = 1000.0
	var ray_to = ray_from + camera.project_ray_normal(mouse_position) * ray_length
	var intercept = plane.intersects_segment(ray_from, ray_to)
	return intercept
	
# when user clicks on the screen in editor mode we implement the add or delete of selected type
# if we click an object we remove it when it matches the tool
# otherwise we place a new object at the grid location
func implement_toolbar(click):
	# cast a camera ray and find click point	
	var ray = get_ray(click)
	
	if ray and editor_active:
		# scan the list to see if we hit anything in it
		var bTookAction = false # record if we actually deleted or selected

		if ray.collider:
			if is_item_in_list_set(ray.collider, select_lists):
				select_item(ray.collider, click)
				bTookAction = true
#			if selected_tool == ToolSelected.Delete and is_item_in_list_set(ray.collider, delete_lists):
#				delete_item(ray.collider)
#				bTookAction = true
#			elif is_item_in_list_set(ray.collider, select_lists):
#				select_item(ray.collider)
#				bTookAction = true

		# if we did not take action then search to see if we hit a map square
		# if we did hit a square we can check if an item is on the square and delete it
		# that would allow user to click under the base of a tree for example and still delete it
		# if the map square is empty we can add the item instead
		if not bTookAction:
			# user has to click on a map square for this section to matter
			for square in squares:
				if square == ray.collider:
#					if selected_tool == ToolSelected.Delete:
#						var item = find_item_at_position(square.transform.origin, delete_lists)
#						if item:
#							delete_item(item)
#							update_sensors()
#							if "Sensor" in item.name:
#								for child in sensor_values:
#									if child['sensor'] == item:
#										sensor_values.erase(child)
#										delete_item(item)
#										update_sensors()
#							else:
#								delete_item(item)
#							bTookAction = true
					if not bTookAction:
						var o = square.transform.origin
						var item = find_item_at_position(o, select_lists)
						if item:
							# special case - if we are trying to place truck and on a road
							# we allow the placement
							if selected_tool != ToolSelected.Truck or get_item_list(item) != roads or find_item_at_position(o, [trucks]) != null:
								select_item(item, click)
								bTookAction = true
					
					if not bTookAction and selected_tool != null:
						var o = ray.position
						var name = tool_to_name(selected_tool)
						var item = place_item(name, o.x, o.z)
						select_item(item, click)

	else: # if not ray or not editor or no selection:
		# we clicked outside the map area
		# we can still potentially have objects lingering out here
		# this happens if you placed road on an 8x8 then change map size to 4x4 for example
		# we leave the objects to avoid deleting work but still allow manual clean up
		# roads don't have collision so they could not be hit with the ray - otherwise we would not need this section
		# but probably we will have other non-colliding objects at some point too
		# find the intercept of the ray with the ground plane y=0
		
		# if we are playing we just register the click so the camera can update the focus
		var intercept = get_ray_to_ground_plane(click)
		if intercept:
			if editor_active and selected_tool == ToolSelected.Delete:
				var item = find_item_at_position(intercept, delete_lists)
				if item:
					pass
					#delete_item(item)
		
			# we want user to easily reset focus of camera by just clicking
			# but it is also a bit distrating when panning camera 
			# so the idea here is that if we clicked far from center of window update orbital target of camera quickly
			# but if we clicked closer to center we can update very slowly
			# this makes panning more natural (tendency to click center)
			# but still allows for quickly changing target of camera (click near edge)
			var window_size = get_viewport().size
			var dxp = (click.x - window_size.x/2) /  (window_size.x/2)
			var dyp = (click.y - window_size.y/2) / (window_size.y/2)
			var p = max(abs(dxp), abs(dyp))
			
			delay_send_camera_event = 1
			set_orbital_camera_target = intercept
			set_orbital_camera_target_percent = p

########################  CAMERA FUNCTİONS ########################

func set_cameras(camera_manager_path: NodePath):
	_camera_manager = camera_manager_path

func cancel_pending_camera_moves():
	delay_send_camera_event = 0

########################  PLACEMENT FUNCTİONS ########################
func origin_to_string_grid_coordinates(object, b3D = false):
	var origin = object.transform.origin
	
	if is_item_in_list_set(object, no_grid_snap_lists):
		if b3D:
			return str(origin.x) + " " + str(origin.z) + " " + str(origin.y)
		else:
			print("Saving coordinates: ", origin)
			return str(origin.x) + " " + str(origin.z)
	else:
		if b3D:
			return str(position_to_grid(origin.x)) + " " + str(position_to_grid(origin.z)) + " " + str(position_to_grid(origin.y))
		else:
			return str(position_to_grid(origin.x)) + " " + str(position_to_grid(origin.z))
			
# world to grid conversion
func position_to_grid(x):
	return stepify(x - world_cell_size/2.0, 1.0)

# grid to world conversion
func grid_to_position(x,y):
	return Vector3(x + world_cell_size/2.0, 0, y + world_cell_size/2.0)

func create_tiles():
	for x in range(world_size):
		for y in range(world_size):
			place_square(x, y)

func set_world_size(set_world_size):
	clear_list(squares)
	world_size = set_world_size
	create_tiles()

# based on string name, call place at x,y on grid
func place_item(name, x, y):
#	tool_selected(null)
#	delete_selection()
	match name:
#		"Drone":
#			return place_drone(x,y)
		"Building1":
			return place_building_1(x,y)
		"Building2":
			return place_building_2(x,y)
		"Building3":
			return place_building_3(x,y)
		"Tree":
			return place_tree(x,y)
		"Square":
			return place_square(x,y)
		"Road":
			return place_road(x,y)
		"Truck":
			return place_truck(x,y)
		"Sensor":
			return place_sensor(x,y)
		"Lamp":
			return place_lamp(x,y)

func place_square(x,y):
	x = position_to_grid(x)
	y = position_to_grid(y)
							
	var square = load("res://assets/Square/Square.tscn").instance()
	squares.append(square)
	square.transform.origin = grid_to_position(x,y)
	call_deferred("add_child", square)

func place_tree(x,y):
	x = position_to_grid(x)
	y = position_to_grid(y)
	var tree = load("res://assets/tree_grown/meshes/tree_grown.tscn").instance()
	tree.transform.origin = grid_to_position(x,y)
	tree.rotation_degrees.y = rng.randf_range(0.0, 360.0)
	trees.append(tree)
	call_deferred("add_child", tree)
	emit_signal("item_placed")
	return tree

func place_lamp(x,y):
	var lamp = load("res://assets/Lamp/Lamp.tscn").instance()
	lamp.transform.origin = Vector3(x,0,y)
	lamps.append(lamp)
	call_deferred("add_child", lamp)
	update_sensors()
	emit_signal("item_placed")
	return lamp
	
func place_building_1(x,y):
	x = position_to_grid(x)
	y = position_to_grid(y)
	var building = load("res://assets/Building/Building.tscn").instance()
	building.transform.origin = grid_to_position(x,y)
	buildings1.append(building)
	call_deferred("add_child", building)
	emit_signal("item_placed")
	return building

func place_building_2(x,y):
	x = position_to_grid(x)
	y = position_to_grid(y)
	var building = load("res://assets/Building/Building2.tscn").instance()
	building.transform.origin = grid_to_position(x,y)
	buildings2.append(building)
	call_deferred("add_child", building)
	emit_signal("item_placed")
	return building

func place_building_3(x,y):
	x = position_to_grid(x)
	y = position_to_grid(y)
	var building = load("res://assets/Building/Building3.tscn").instance()
	building.transform.origin = grid_to_position(x,y)
	buildings3.append(building)
	call_deferred("add_child", building)
	emit_signal("item_placed")
	return building

func place_sensor(x,y):
	var sensor = load("res://assets/Sensor/Sensor.tscn").instance()
	sensor.transform.origin = Vector3(x,0,y)
	sensors.append(sensor)
	add_child(sensor)
	sensor.set_range(min_sensor_value, max_sensor_value)
	var letter = set_letter(sensor)
	update_sensor(sensor, letter)
	emit_signal("item_placed")
	return sensor

func place_road(x,y):
	x = position_to_grid(x)
	y = position_to_grid(y)
	var road = load("res://assets/Road/Road.tscn").instance()
	road.transform.origin = grid_to_position(x,y)
	roads.append(road)
	call_deferred("add_child", road)
	update_road_orientations()
	emit_signal("item_placed")

func place_truck(x,y):
	x = position_to_grid(x)
	y = position_to_grid(y)
	var truck = load("res://assets/Vehicles/Truck.tscn").instance()
	truck.transform.origin = grid_to_position(x,y)
	trucks.append(truck)
	call_deferred("add_child", truck)
	update_truck_road_lists()
	emit_signal("item_placed")

func finalize_level_setup():
	create_tiles()

	var camera = get_tree().get_root().get_node(camera_node_path)
	#camera.event_new_sceneario_loaded()

func set_letter(sensor) -> String:
	var letter_num = 65
	if len(sensors) <= 26:
		# 65 is correspond to 'A' due to ASCII table
		letter_num += sensors.find(sensor)
	elif len(sensors) <= 52:
		# 97 is correspond to 'a' due to ASCII table
		letter_num = 97 + sensors.find(sensor)
	else:
		# TO DO set limits for sensor, Out of letter :D
		letter_num = 0
		
	var letter_char = PoolByteArray([letter_num]).get_string_from_ascii()
	return letter_char

########################  UPDATE FUNCTİONS ########################
func update_sensors():
	for sensor in sensors:
		var letter = set_letter(sensor)
		sensor.set_range(min_sensor_value, max_sensor_value)
		update_sensor(sensor, letter)

func update_sensor(sensor, letter):
	sensor.set_lamp_list(lamps, letter)
	emit_signal("update_sensor_values", sensor, letter)
	
# roads automatically pick horizontal, vertical, or curved based on neighbors
# currently we just update everything anytime a road is added or removed
# if roads have too many neighbors to decide this will make an abritrary choice based on ordering in the list
func update_road_orientations():
	for ai in roads.size(): # loop 1
		var a = roads[ai]
		
		# track if another road is left/right (dx) or up/down (dy) or both
		var dx = 0
		var dy = 0

		for bi in roads.size(): # loop 2
			if bi != ai: # don't check against select
				var b = roads[bi]
				# are they touching?
				var delta = b.transform.origin - a.transform.origin
				if abs(delta.x) < world_cell_size * 1.5 and abs(delta.z) < world_cell_size * 1.5: # close
					if abs(delta.x) < world_cell_size * 0.5 or abs(delta.z) < world_cell_size * 0.5: # and not diagonal
						# track kinds of neighbors we have
						# note if we just have left (dx=-1) and right (dx=1) then dx will overwrite previous value on second hit
						# but then we don't care - it is horizontal road and important thing is dy=0
						# so sign of dx and dy only matters if we set both and therefore it is a corner which must be rotated properly
						if delta.x > 0:
							dx = 1
						if delta.x < 0:
							dx = -1
						if delta.z > 0:
							dy = 1
						if delta.z < 0:
							dy = -1

		# get grid coordinates
		var x = position_to_grid(a.transform.origin.x)
		var y = position_to_grid(a.transform.origin.z)
		
		# decide road type - if dx != 0 and dy != 0 then we use a corner
		var path = "res://assets/Road/Road.tscn"
		if dx != 0 and dy != 0:
			path = "res://assets/Road/Corner.tscn" # corner
		
		# make the road item
		# this section is very inefficient since we always delete and replace
		# TODO: clean this up so we only make changes when necessary
		# almost all of the roads are not going to change
		# we need to look at road and determine if it is already the rigght type (in Godot how do we check instance name or do we have to add a variable script?)
		var road = load(path).instance()						
		road.transform.origin = grid_to_position(x,y)
		roads[ai] = road # replace
		a.queue_free()
		call_deferred("add_child", road)

		if dx != 0 and dy != 0: # is it a corner
			if dx == 1 and dy == 1:
				road.rotation_degrees.y = 0
			elif dx == 1 and dy == -1:
				road.rotation_degrees.y = 90
			elif dx == -1 and dy == 1:
				road.rotation_degrees.y = 270
			elif dx == -1 and dy == -1:
				road.rotation_degrees.y = 180
		elif dx != 0:
			road.rotation_degrees.y = 0
		elif dy != 0:
			road.rotation_degrees.y = 90
	
	update_truck_road_lists()

func update_truck_road_lists():
	for truck in trucks:
		truck.update_roads(roads)

func update_game(dt):
	update_camera_target_update()

func update_camera_target_update():
	if delay_send_camera_event > 0:
		delay_send_camera_event -= 1
		if delay_send_camera_event <= 0:
			var camera = get_tree().get_root().get_node(camera_node_path)
			delay_send_camera_event = 0
			#camera.event_set_new_orbital_focus(set_orbital_camera_target, set_orbital_camera_target_percent)


########################  LIST & FILE FUNCTİONS ########################
func open_scenario_as_text_file(file_name):
	loaded_file_name = file_name

	var set_file_name_text_edit = get_tree().get_root().get_node("DirtRiderScene/VBoxContainer/HBoxContainer3/FileName")
	set_file_name_text_edit.text = file_name
	
	for a in all_lists:
		clear_list(a)	

	var file = File.new()
	file.open(scenario_file_path + file_name + ".gd", File.READ)
	
	world_size = 8 # default

	while not file.eof_reached(): # iterate through all lines until the end of file is reached
		var line = file.get_line()
		
		var sub_line = line.split(" ", false) # note false is to prevent empty string
		var base_string_count = 3
		if sub_line.size() > 0:
			var name = sub_line[0]
			
			if name == "MapSize":
				world_size = float(sub_line[1])
			elif sub_line.size() >= base_string_count:
				var x = float(sub_line[1])
				var y = float(sub_line[2])
				var item = place_item(name, x, y)
				
				var extra_strings = []
				for i in range(sub_line.size() - base_string_count):
					var add_string = sub_line[i+base_string_count]
					extra_strings.append(add_string)
				if extra_strings.size() > 0:
					item.read_strings(extra_strings)
																							
	file.close()

	finalize_level_setup()

func save_scenario_as_text_file(file_name):
	
	loaded_file_name = file_name

	var file = File.new()
	file.open(scenario_file_path + file_name + ".gd", File.WRITE)
	
	var content = ""
	
	var new_line = "MapSize " + str(world_size) + "\n"
	file.store_string(new_line)
	
	# I change select_lists to list_for_save_file because when we are not in developer mode
	# give us a error
	for i in range(list_for_save_file.size()):
		content += write_list(file, list_for_save_file[i], select_list_names[i])

	file.store_string(content)
	file.close()

func return_hard_code_map(file_name):
	var file = File.new()
	file.open(scenario_file_path + file_name + ".gd", File.WRITE)
	
	file.store_string("\"\"\"\n") # start comment block so we don't have warnings for file
	var content = ""
	
	var new_line = "MapSize " + str(world_size) + "\n"
	file.store_string(new_line)
	content += "Tree 7 0\n"
	content += "Tree 0 0\n"
	content += "Tree 1 0\n"
	content += "Tree 2 0\n"
	content += "Road 1 7\n"
	content += "Road 1 6\n"
	content += "Road 1 5\n"
	content += "Road 1 4\n"
	content += "Road 1 3\n"
	content += "Road 2 2\n"
	content += "Road 3 2\n"
	content += "Road 4 2\n"
	content += "Road 4 3\n"
	content += "Road 4 4\n"
	content += "Road 4 5\n"
	content += "Road 4 6\n"
	content += "Road 4 7\n"
	content += "Road 3 7\n"
	content += "Road 2 7\n"
	content += "Truck 2 7\n"
	content += "Sensor 6.598886 7.293218\n"
	content += "Sensor 7.168481 5.041666\n"
	content += "Sensor 2.840306 5.496778\n"
	content += "Lamp 5.506172 2.631827 0\n"
	content += "Lamp 5.509279 4.637715 0\n"
	content += "Lamp 2.680491 5.055672 0\n"
	content += "Lamp 7.536036 6.720954 0\n"
	content += "Lamp 7.199682 7.499693 0\n"
	file.store_string(content)
	file.store_string("\"\"\"\n") # end comment block so we don't have warnings for file
	file.close()

func save():
	var dialog = ConfirmationDialog.new()
	get_tree().get_root().add_child(dialog)
	var set_file_name_text_edit = get_tree().get_root().get_node("DirtRiderScene/VBoxContainer/HBoxContainer3/FileName")
	var name_to_save = set_file_name_text_edit.text
	
	if name_to_save == "":
		# TODO make a dialog - Please enter a file name
		return
		
	var bConfirm = false
	var current_files = list_names_in_popup()
	for file in current_files:
		if file == name_to_save:
			bConfirm = true
			break
	
	waiting_to_save_file_name = name_to_save
	
	if bConfirm and developer_mode:
		var confirm = get_tree().get_root().get_node("DirtRiderScene/ConfirmationSave")
		confirm.dialog_text = name_to_save
		confirm.popup()
	else:
		save_file_confirmed()
	#send file to riders
	emit_signal("item_placed")

func save_file_confirmed():
	cancel_pending_camera_moves()
	save_scenario_as_text_file(waiting_to_save_file_name)
	setup_scenario_menu()

# connect a button the corresponding list
func tool_to_list(tool_id):
	match tool_id:
		ToolSelected.Tree:
			return trees
		ToolSelected.Building1:
			return buildings1
		ToolSelected.Building2:
			return buildings2
		ToolSelected.Building3:
			return buildings3
		ToolSelected.Road:
			return roads
		ToolSelected.Sensor:
			return sensors
	return null

func get_item_list(item):
	for list in all_lists:
		for i in list:
			if i == item:
				return list
	return null

func get_item_index(list, item):
	for i in range(list.size()):
		if list[i] == item:
			return i
	return null

func delete_item(item):
	if is_instance_valid(item):
		if "Sensor" in item.name:
			for child in sensor_values:
				if child['sensor'] == item:
					sensor_values.erase(child)
		if "Lamp" in item:
			update_sensors()
		var list = get_item_list(item)
		remove_list_world_item(list, item)
		
# remove item from world and also from the corresponding list
# this includes two special cases:
#  (2) for roads - we should update the road orientations when we delete a road
func remove_list_world_item(list, item):
	var index = get_item_index(list, item)
	
	if index == null:
		assert(false) # do fail
	list[index].queue_free()
	list.remove(index)
	# special cases
	if roads.size() != 0 and list == roads:
		update_road_orientations()

func clear_list(items):
	for item in items:
		item.queue_free()
	items.clear()

func write_list(file, list, name):
	var content = ""
	for item in list:
		content += name + " " + origin_to_string_grid_coordinates(item) + " "
		content += item.write_strings()
		content += "\n"
	return content

func find_item_at_position(position, list_set):
	# determine the grid square that would be here even if map square does not actually exist as an object
	var x = stepify(position.x, 0.5)
	var z = stepify(position.z, 0.5)
	for list in list_set:
		for item in list:
			var delta = item.transform.origin - Vector3(x,0,z)
			delta.y = 0
			if delta.length() < world_cell_size/2.0: # since (x,z) is locked to center and items are placed at center, length should be ~0 for a match
				return item
	return null
	
func is_item_in_list_set(item, list_set):
	for list in list_set:
		if is_item_in_list(item, list):
			return true
	return false

func is_item_in_list(item, list):
	for i in list:
		if i == item:
			return true
	return false
	
# this is used to collect all files and make a menu for user to pick scenarios
func list_files_in_directory(path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
	dir.list_dir_end()
	return files

func toggle_editor():
	cancel_pending_camera_moves()
		
	editor_active = !editor_active
	
	if editor_active:
		pause()
	else:
		select_item(null) # remove the selection item
		start()
	
	var scene_name = "DirtRiderScene"
	if editor_active:
		for name in tool_node_names:
			if name in vbox_tool_node_name:
				get_tree().get_root().get_node(scene_name + "/VBoxContainer/HBoxContainer3/" + name).show()
			else:
				get_tree().get_root().get_node(scene_name + "/" + name).show()
	else:
		for name in tool_node_names:
			if name in vbox_tool_node_name:
				get_tree().get_root().get_node(scene_name + "/VBoxContainer/HBoxContainer3/" + name).show()
			else:
				get_tree().get_root().get_node(scene_name + "/" + name).show()

# make the popup menu have the names of the scenario files so user can select them
func setup_scenario_menu():
	var menu = get_tree().get_root().get_node("DirtRiderScene/VBoxContainer/HBoxContainer3/ScenarioMenu")
	var names = list_names_in_popup()
	var popup = menu.get_popup()
	popup.clear()
	for name in names:
		popup.add_item(name)

# get all the names in the popup menu for loading scenario files
func list_names_in_popup():
	#scenario_file_path = "user://logs"
	var files = list_files_in_directory(scenario_file_path)
	var names = []
	for file in files:
		var sub_str = file.split(".")
		if sub_str.size() == 2 and sub_str[1] == 'gd':
			names.append(sub_str[0])
	return names

########################  STATE FUNCTİONS ########################
func pause():
	# If simulation is not already running, cannot pause it.
	if _simulation_state != SimulationState.RUNNING:
		return
	_set_state(SimulationState.PAUSED)
	
func start():
	# If simulation is not ready, cannot start it.
	if _simulation_state != SimulationState.READY and _simulation_state != SimulationState.PAUSED:
		return
	_set_state(SimulationState.RUNNING)

func restart():
	# If simulation is not already running or paused, cannot restart it.
	if _simulation_state != SimulationState.RUNNING and _simulation_state != SimulationState.PAUSED and _simulation_state != SimulationState.ENDED:
		return
	
	_set_state(SimulationState.RESTARTING)
	
	# To set last camera as initial camera, current index is being saved.
	# Index will be used in the "_ready" function of the CameraManager script.
	if _camera_manager != null:
		camera_index = get_node(_camera_manager).current_index
	
	get_tree().reload_current_scene()
	
	_set_state(SimulationState.READY)

func toggle_start():
	if _simulation_state == SimulationState.RUNNING:
		pause()
	else:
		start()

func is_running() -> bool:
	return _simulation_state == SimulationState.RUNNING

func is_paused():
	return _simulation_state == SimulationState.PAUSED

func _init():
	_set_state(SimulationState.INITIALIZING)
	rng.randomize()

func _set_state(state):
	_simulation_state = state
	emit_signal("change_in_sim_state", _simulation_state)

func get_url():
	var host_name = JavaScript.eval('window.location.hostname')
	if host_name:
		return ("https://" + host_name)

		
# Called when the node enters the scene tree for the first time.
func _ready():
	if OS.get_name() == "HTML5":
		scenario_file_path = scenario_file_path_export
		var directory = Directory.new()
		if not directory.file_exists(scenario_file_path):
			directory.make_dir_recursive(scenario_file_path)
			get_map_file_url = get_url()+"/get-map-file/"
			send_map_file_url = get_url()+"/send-map-file/"
			
	else:
		get_map_file_url = "http://127.0.0.1:8000/get-map-file/"
		send_map_file_url = "http://127.0.0.1:8000/send-map-file/"
	
	GameEvaluator.connect("finish_game", self, "_on_finish_game_signal")
	_set_state(SimulationState.READY)
	setup_scenario_menu()
	start()
	toggle_editor()


func execute_drag_item(mouse_position):	# cast a camera ray and find click point	
	if dragging_item != null:
		var intercept = get_ray_to_ground_plane(mouse_position)
		dragging_item.transform.origin.x = intercept.x
		dragging_item.transform.origin.z = intercept.z
		dragging_item.transform.origin += dragging_item_offset
		update_sensors()
		
func _unhandled_input(event):
	if event is InputEventMouseMotion:
		if dragging_item:
			dragging_state = true
			execute_drag_item(event.position)

	# Mouse in viewport coordinates.
	if event is InputEventMouseButton:
		if not event.pressed:
			dragging_item = null
			if dragging_state:
				# if dragging finished updated map file
				emit_signal("item_placed")
				dragging_state = false
		if event.pressed:
			# note we do the click for playing too
			# we always tell the camera so we can update the orbital camera focus
			implement_toolbar(event.position)
			
func _physics_process(delta):
	if not is_running():
		return
	# Process python every _update_python_frequency frames
	# Otherwise, do nothing and continue to the next frame
	if _update_python_frequency_counter != _update_python_frequency:
		_update_python_frequency_counter += 1
		return
	_update_python_frequency_counter = 0
	# Notify all sensors for them to update their states to their js msg definitions.
	emit_signal("update_sensors")
	# Notify CommunicationManager to notfy javascript about the new sensor states.
	emit_signal("godot_is_ready_for_processing")

	update_game(delta)

########################  SIGNAL HANDLER FUNCTİONS ########################
func on_godot_notified_by_javascript():
	emit_signal("vehicle_cmd_msg_js_change")

func _on_finish_game_signal():
	# If simulation has not already started, cannot end it.
	if _simulation_state != SimulationState.RUNNING and _simulation_state != SimulationState.PAUSED:
		return

	_set_state(SimulationState.ENDED)
