extends Node

# velocity
var maximum_speed = 5.0 # maximum allowed total speed
var force_gain = 1.0

# angular velocity
var maximum_angular_velocity = 2.0 * PI # maximum allowed angular velocity
var torque_gain = 0.4

func _init():
	pass # Replace with function body.

func update(robot, dt, target_velocity : Vector3, target_angular_velocity : float, energy):
	if GameManager.is_paused():
		return # need to decide how to implement this clean with Godot

	if energy <= 0:
		robot.add_force(Vector3(0,-9.8,0), Vector3(0,0,0))
		robot.linear_damp = 0
		robot.angular_damp = 0
		return
		
	# clamps
	target_velocity.x = clamp(target_velocity.x, -maximum_speed, maximum_speed)
	target_velocity.y = clamp(target_velocity.y, -maximum_speed, maximum_speed)
	target_velocity.z = clamp(target_velocity.z, -maximum_speed, maximum_speed)
	
	target_angular_velocity = clamp(target_angular_velocity, -maximum_angular_velocity, maximum_angular_velocity)

	var b = robot.transform.basis
	var m = target_velocity
	var target_velocity_in_basis = b.x * m.x + b.y * m.y + b.z * m.z

	# do physics
	update_speed(robot, dt, target_velocity_in_basis)
	update_angular_velocity(robot, dt, target_angular_velocity)
	update_visual_tilt(robot)

func maintain_vertial_tilt(robot):
	var basis = robot.transform.basis
	robot.add_torque(Vector3(0, 0, -robot.rotation_degrees.z * 0.5))
	
func update_visual_tilt(robot):
	# cosmetic - make drone tilt in direction of movement
	var basis = robot.transform.basis

	var x_vel = robot.linear_velocity.dot(basis.x)
	var z_vel = robot.linear_velocity.dot(basis.y)
	
	var maximum_tilt = 20.0
	var tilt_to_velocity_ratio = 10.0
	var maintain_tilt_gain = 0.05

	var target_tilt_z = -x_vel * tilt_to_velocity_ratio
	if target_tilt_z > maximum_tilt:
		target_tilt_z = maximum_tilt
	if target_tilt_z < -maximum_tilt:
		target_tilt_z = -maximum_tilt

	var target_tilt_x = -z_vel * tilt_to_velocity_ratio
	if target_tilt_x > maximum_tilt:
		target_tilt_x = maximum_tilt
	if target_tilt_x < -maximum_tilt:
		target_tilt_x = -maximum_tilt
		
	var tz = (target_tilt_z - robot.rotation_degrees.z) * maintain_tilt_gain
	var tx = (target_tilt_x - robot.rotation_degrees.x) * maintain_tilt_gain

	robot.add_torque(basis.x * tx + basis.z * tz)

func update_speed(robot, dt, target_velocity):	
	var delta = target_velocity - robot.linear_velocity
	robot.add_force(delta * force_gain, Vector3(0,0,0))

func update_angular_velocity(robot, dt, target_angular_velocity):		
	var delta = target_angular_velocity - deg2rad(robot.angular_velocity.y)
	robot.add_torque(Vector3(0, delta * torque_gain, 0))
