## 
## ScoutRider Physics
## 

extends 'res://addons/Robots/RobotComponents/RobotBaseComponent.gd'

const robot_physics_script = preload("RobotPhysics.gd")

# Temporary user code variables - later these will live in the python user code
var charging = false # once we start charging we stay with it until we hit max

# are we in continuous or step mode
# TODO - this will automatically set based on what commands user is making
var is_continuous_mode = false # if true user controls movemennt - otherwise just gives target

# internal
var crashed = false # once we run out of energy we can never recover
var step_target_position : Vector3
var step_target_angle = 0.0
var grid_size = 1.0
var current_package = null
var implementing_step_move_time = 0.0 # if step motion and this is > 0 - user has no control until step is over
var step_move_time = 1.0
var physics_move : Vector3
var physics_turn = 0.0
var robot_physics = robot_physics_script.new()
var ray_length = 1000.0 # for distance sensors
var ray_offset = 0.215 # how far from body center ray starts
var rotor_rotation = 0.0 # tracks cosmetic rotor rotation angle
var distance_for_charging = 0.5
var charger_land_offset = 0.5 # height coordinate for being on charger
var start_land_offset = 1.5 # height coordinate for picking up or delivering package at start/finish

# drone properties - not clear yet where these will live - they will not be the same for all drones
var max_energy = 20.0
var energy = max_energy
var charging_speed = 3.0
var use_energy_speed = 1.0

var temporary_move_counter = 0

var height_key = "height"
var rotation_key = "rotation"
var max_energy_key = "energy"

func read_strings(strings):
	# interdependency design here is not good but not sure yet how we are going to define
	# all the extra file write options for each objects - probably should be in the object script
	# so will sort this out better later
	
	# for now we are just setting drone position height and rotation
	# note there is no editor option yet to do this
	# hard coding them into the file
	for s in strings:
		var list = s.split(":", false)
		if list.size() == 2:
			var key = list[0]
			var value = float(list[1])
			
			match key:
				height_key:
					transform.origin.y = (value + 0.5) * GameManager.world_cell_size
				rotation_key:
					rotation_degrees.y = value
				max_energy_key:
					set_max_energy(value)

func set_max_energy(value):
	energy = value
	max_energy = value

func write_strings():
	var result = ""
	var y_pos_str = str(stepify((transform.origin.y - GameManager.world_cell_size * 0.5)/GameManager.world_cell_size, 1.0))
	result += height_key + ":" + y_pos_str
	result += " "
	result += rotation_key + ":" + str(rotation_degrees.y)
	result += " "
	result += max_energy_key + ":" + str(max_energy)
	return result

# methods the user will have access to
func user_turn_right():
	do_step_turn(-PI/2.0)

func user_turn_left():
	do_step_turn(PI/2.0)

func user_move_forward():
	do_step_move(user_get_x_axis())

func user_move_backward():
	do_step_move(user_get_x_axis() * -1.0)

func user_move_right():
	do_step_move(user_get_z_axis())

func user_move_left():
	do_step_move(user_get_z_axis() * -1.0)

func user_move_up():
	do_step_move(Vector3(0,1,0))

func user_move_down():
	do_step_move(Vector3(0,-1,0))

# user doesn't need this really - they just do nothing in step mode
# but for the exmaple temp code here it's useful to have a placeholder to keep things clear
func user_do_nothing():
	do_step_move(Vector3(0,0,0))

func user_package(): # package we are carrying, if any
	return current_package

func user_move(move):
	physics_move = move

func user_turn(turn):
	physics_turn = turn
	
func user_energy(): # current energy level
	return energy
	
func user_max_energy(): # current max energy level
	return max_energy
	
func user_get_starts(): # returns a list of all start positions for delivery routes
	return GameManager.starts

func user_get_ends(): # returns a list of all end positions for delivery routes - length and indices align with starts for one to one
	return GameManager.ends

func user_start_height_offset(): # vertical offset from start/end for pickup/delivery
	return start_land_offset
	
func user_get_chargers(): # returns a list of charging stations
	return GameManager.chargers

func user_distance_for_charging(): # how close we have to be to get charging
	# this will be measured from charger position with vertical offset of user_charger_height_offset()
	return distance_for_charging

func user_charger_height_offset(): # vertical offset from charger for charging
	return charger_land_offset

func user_get_packages(): # returns a list of all packages - length and indices match starts for one to one correspondence
	return GameManager.packages

func user_drop_package(): # user command to drop currently held package
	GameManager.drone_drop_package(current_package)
	current_package = null

func user_grab_package(): # user command to grab package - does nothing if already carrying a package
	if current_package == null:
		current_package = GameManager.drone_grab_package(self) # user code send command to manager to link up with package

func user_distance_for_grab_package(): # how close we need to be to grab package
	return GameManager.grab_package_distance

func user_distance_for_drop_package(): # how close we need to be to drop package
	return GameManager.grab_package_distance # right now just making this the same

func user_get_x_axis():
	return transform.basis.x

func user_get_y_axis():
	return transform.basis.y

func user_get_z_axis():
	return transform.basis.z

func user_is_package_at_start(start): # utility method users could implement themselves
	var index = user_get_index(start, user_get_starts())
	if index != -1:
		var package = user_get_packages()[index]
		var delta = start.transform.origin + Vector3(0,start_land_offset,0) - package.transform.origin
		if delta.length() < user_distance_for_grab_package() * 3.0:
			return true # the package is available
	return false

func user_get_index(item, list): # utility method users could implement themselves
	for i in range(list.size()):
		if list[i] == item:
			return i
	return -1

# temporary methods part of the temporary user code
# these will eventually be deleted and the user implements in their own code

# calculate manhatten distance between two points
func manhatten(v):
	return abs(v.x) + abs(v.y) + abs(v.z)

# utility method to find the closest object to origin (ignoring height)
func get_closest(origin, list):
	var best = null
	var best_dist = 0
	for item in list:
		var delta = item.transform.origin - origin
		delta.y = 0
		var dist = manhatten(delta)
		if best_dist == 0 or dist < best_dist:
			best_dist = dist
			best = item
	return best

# utility method to find the closest start with a valid package
func get_closest_start(origin):
	var best_start = null
	var best_dist = 0
	for start in user_get_starts():
		var delta = start.transform.origin - origin
		delta.y = 0
		var dist = manhatten(delta)
		if best_dist == 0 or dist < best_dist:
			if user_is_package_at_start(start):
				best_dist = dist
				best_start = start
	return best_start

# utility method - get index of finish which matches package
func get_matching_finish(package):
	for i in range(user_get_packages().size()):
		if user_get_packages()[i] == package:
			if i < user_get_packages().size():
				return GameManager.ends[i]
	return null
	
# determine next target - either a pickup point or a drop off point
func calculate_target():
	if user_package() == null:
		var start = get_closest_start(transform.origin)
		if start:
			return start.transform.origin + Vector3(0,start_land_offset,0)
	else:
		var finish = get_matching_finish(user_package())
		if finish:
			return finish.transform.origin + Vector3(0,start_land_offset,0)
	return null

func decide_plan():
	var target = calculate_target()
	if target == null:
		return
	
	# pick charger if we will run out of battery before we get to the destination and then to the charger
	if charging and energy >= max_energy:
		charging = false # done charging
		
	if charging:
		target = get_closest(transform.origin, user_get_chargers()).transform.origin + Vector3(0,charger_land_offset,0)
	else:
		var delta = target - transform.origin
		var distance_to_destination = manhatten(delta)
		var charger = get_closest(target, user_get_chargers())
		if charger:
			var distance_target_to_charger = manhatten(charger.transform.origin + Vector3(0,user_charger_height_offset(),0) - target)
			var total_distance_to_charge = distance_to_destination + distance_target_to_charger
			# right now we assume speed is 1.0 
			# currently moving faster doesn't cost more so this will be adjusted by user
			var total_time_to_charge = total_distance_to_charge / 1.0
			var energy_buffer = 2.0
			if total_time_to_charge * use_energy_speed > user_energy() - energy_buffer:
				charging = true
	
	# now make a height adjustmnet
	var delta = target - transform.origin
	var dist = delta.length()
	var dy = delta.y
	delta.y = 0
	var xz_dist = delta.length()
	
	if xz_dist > 0.5:
		target.y = 2.5 # fly high until we get within range of the target

	if not charging:
		if user_package() == null:
			if dist < user_distance_for_grab_package()/4.0: # factor of 4 make sure we are in zone
				user_grab_package()
		else:
			if dist < user_distance_for_drop_package()/4.0: 
				user_drop_package()

	return target
		
func temporary_implement_solution_DownStreet1():
	match temporary_move_counter:
		0: user_move_forward()
		1: user_move_forward()
		2: user_move_forward() # collect

func temporary_implement_solution_DownStreet2():
	match temporary_move_counter:
		0: user_move_forward()
		1: user_move_forward()
		2: user_move_forward()
		3: user_turn_left()
		4: user_move_forward() # collect

func temporary_implement_solution_DownStreet3():
	match temporary_move_counter:
		0: user_move_forward()
		1: user_move_forward()
		2: user_move_forward()
		3: user_turn_right()
		4: user_move_forward()  # charge is 5
		5: user_move_backward()
		6: user_move_backward()
		7: user_move_backward()
		8: user_move_backward() # collect

func temporary_implement_solution_DownStreet4():
	match temporary_move_counter:
		0: user_move_forward()
		1: user_move_forward()
		2: user_move_forward()
		3: user_turn_right()
		4: user_move_forward()  # charge is 5
		5: user_move_backward()
		6: user_move_backward()
		7: user_move_backward()
		8: user_turn_right()
		9: user_move_forward()
		10: user_do_nothing()
		11: user_move_forward()
		12: user_turn_right()
		13: user_move_forward()
		14: user_turn_right()		
		15: user_move_forward()
		16: user_move_forward()
			
# this code would be replaced by python code later
func temporary_hard_coded_algorithm():
	# if we have an example solution for the level implement it
	match GameManager.loaded_file_name:
		"DownStreet1":
			temporary_implement_solution_DownStreet1()
		"DownStreet2":
			temporary_implement_solution_DownStreet2()
		"DownStreet3":
			temporary_implement_solution_DownStreet3()
		"DownStreet4":
			temporary_implement_solution_DownStreet4()
		_: # default case runs a generic algorithm - look for packages and delivery them - whatever is closest
			var target = decide_plan()
			if target:
				var delta = target - transform.origin
				var angle = -atan2(delta.z, delta.x)
				if is_continuous_mode: # otherwise do the generic continuous code
					temporary_implement_move(target, angle)
				else: # or step code depending on the level mode
					temporary_implement_step_move(target, angle)
			
	if not is_continuous_mode:
		temporary_move_counter += 1

func temporary_implement_move(target, angle):
	# user will not have this - they must write their own
	# but for internal demo code we have it to implement step motion so I just use it again
	implement_move(target, angle)
		
func snap_position_to_grid(v):
	var pos = Vector3(v)
	var h = grid_size * 0.5
	pos.x = stepify(pos.x - h, 1.0) + h
	pos.y = stepify(pos.y - h, 1.0) + h
	pos.z = stepify(pos.z - h, 1.0) + h
	return pos

func snap_angle_to_grid(angle):
	return stepify(angle, PI/2.0)

# user implements this logic and uses simple commands - like forward, backward, right, left
func temporary_implement_step_move(target, target_angle):
	# lock the target to the grid
	snap_position_to_grid(target)
	var delta = target - transform.origin
	
	# lock the angle to the grid
	target_angle = snap_angle_to_grid(target_angle)
	var delta_angle = get_delta_angle(deg2rad(rotation_degrees.y), target_angle)

	# do angle first, then up/down, then forward - though user could do lateral moves as well and ignore angle
	var h = grid_size * 0.5
	
	if delta_angle > PI/4.0:
		user_turn_left()
	elif delta_angle < -PI/4.0:
		user_turn_right()
	elif delta.y > h:
		user_move_up()
	elif delta.y < -h:
		user_move_down()
	elif delta.dot(user_get_x_axis()) > h:
		user_move_forward()
	else:
		do_step_move(Vector3(0,0,0)) 

func get_delta_angle(a, b):
	# get the relative angle
	var delta_angle = b - a
	while delta_angle > PI:
		delta_angle -= 2.0 * PI
	while delta_angle < -PI:
		delta_angle += 2.0 * PI
	return delta_angle

func implement_move(target, target_angle):
	# handle rotation
	var delta_angle = get_delta_angle(deg2rad(rotation_degrees.y), target_angle)
	var turn_gain = 4.0
	user_turn(delta_angle * turn_gain)

	# handle movement
	var move = Vector3(0, 0, 0) # movement speed in xyz - if turn based this will be integers
	var delta = target - transform.origin
	var speed_gain = 3.0	
	
	# this needs to be large enough so robot can complete the move in specified time
	# currently 1s to complete 1 unit move
	# we want the time per step move to be constant so it makes sense that energy loss is 1
	# but maybe this all needs to change later anyways
	# it is an awkward design right now
	if not is_continuous_mode:
		speed_gain = 6.0
	
	move.x = delta.dot(user_get_x_axis()) * speed_gain
	move.y = delta.dot(user_get_y_axis()) * speed_gain
	move.z = delta.dot(user_get_z_axis()) * speed_gain
	user_move(move)

func do_step_turn(radians):
	implementing_step_move_time = step_move_time
	# lock our current rotation to grid
	var current_rotation = snap_angle_to_grid(deg2rad(rotation_degrees.y))
	# we don't need to lock radians because it should already be a snapped value
	step_target_angle = current_rotation + radians
	step_target_position = snap_position_to_grid(transform.origin)
	
func do_step_move(delta):
	implementing_step_move_time = step_move_time
	var origin = snap_position_to_grid(transform.origin)
	step_target_angle = snap_angle_to_grid(deg2rad(rotation_degrees.y))
	step_target_position = origin + delta

func implement_step_move(delta):
	implement_move(step_target_position, step_target_angle)
	
	var dist = (step_target_position - transform.origin).length()
	var angle_delta = get_delta_angle(deg2rad(rotation_degrees.y), step_target_angle)
	
	# this determines how close step move is before we allow next step move to begin
	implementing_step_move_time -= delta
	if implementing_step_move_time <= 0:
		implementing_step_move_time = 0 # can calculate new move now
		
		# clear settings because user might not do anything so we should stop
		user_move(Vector3(0,0,0))
		user_turn(0)
		
		# apply energy loss or gain
		if not crashed:
			if is_on_charger():
				energy += step_move_time * charging_speed
				if energy > max_energy:
					energy = max_energy
			else:
				energy -= step_move_time * use_energy_speed
				if energy < 0:
					energy = 0

func _physics_process(delta: float):
	if  GameManager.is_running():
		if implementing_step_move_time > 0:
			implement_step_move(delta)
		if is_continuous_mode or implementing_step_move_time == 0:
			if not is_continuous_mode:
				user_do_nothing() # by default set command to do nothing - we will burn energy if we do nothing
			temporary_hard_coded_algorithm() # to be replaced by user code
		if current_package != null:
			tow_package(current_package)
		if energy <= 0:
			crashed = true
		robot_physics.update(self, delta, physics_move, physics_turn, energy)
		if energy > 0:
			update_rotors(delta)
		update_energy_bar(delta)
		if is_continuous_mode: # continuous mode does charging in real time but step mode does it in steps
			update_charging(delta) # do this after update_energy_bar for clean hit to max
	draw_energy_bar()
	
func tow_package_line(package, p_offset, d_offset):
	var ro = transform.origin + d_offset
	var po = package.transform.origin + p_offset
	var F = 10.0
	var delta = ro - po
	package.add_force(delta * F, p_offset )
	
func tow_package(package):
	# we apply a force so the package is towed
	var p_basis = package.transform.basis
	var d_basis = transform.basis
	var r = 0.2
	var r2 = 0.5
	tow_package_line(package, p_basis.x * r, d_basis.x * r2)
	tow_package_line(package, p_basis.x * -r, d_basis.x * -r2)
	tow_package_line(package, p_basis.z * r, d_basis.z * r2)
	tow_package_line(package, p_basis.z * -r, d_basis.z * -r2)

func is_on_charger():
	for charger in GameManager.chargers:
		var delta = charger.transform.origin + Vector3(0,charger_land_offset,0) - transform.origin
		if manhatten(delta) < distance_for_charging:
			return true
	return false

func update_charging(dt):
	if is_on_charger():
		energy += dt * charging_speed
		if energy > max_energy:
			energy = max_energy

func draw_energy_bar():
	get_node("HealthBar3D").update(transform.origin, energy, max_energy)

func update_energy_bar(delta):
	if is_continuous_mode:
		energy -= delta * use_energy_speed
	if energy < 0:
		energy = 0

func update_rotors(delta):
	# set wheel spins
	rotor_rotation += delta * 360.0 * 8.0
	if rotor_rotation > 360.0:
		rotor_rotation -= 360.0
	if rotor_rotation < 0:
		rotor_rotation += 360.0
	get_node("ringrider_propeller1").rotation_degrees.y = rotor_rotation
	get_node("ringrider_propeller2").rotation_degrees.y = rotor_rotation
	get_node("ringrider_propeller3").rotation_degrees.y = rotor_rotation
	get_node("ringrider_propeller4").rotation_degrees.y = rotor_rotation
		
func on_vehicle_cmd_msg_js_change():
	pass
	# TODO make move xyz
	#move = vehicle_cmd_msg_js.move
	#turn = vehicle_cmd_msg_js.turn

func _ready():
	if name == 'DirtRider':
		EngineInputController.robot = self
		GameManager.robot = self # temp hack for follow camera - need to decide general design for this
		update_energy_bar(0.0)

func get_distance_sensor(path):
	var space_state = get_world().direct_space_state
	var v = path.normalized()
	var begin = global_transform.origin + v * ray_offset
	var ray = get_world().direct_space_state.intersect_ray(begin, begin + v * ray_length, [self, get_parent()])
	if ray:
		return begin.distance_to(ray.position)
	return 1000.0
	
func get_right_distance_sensor():
	return get_distance_sensor(global_transform.basis.z)

func get_left_distance_sensor():
	return get_distance_sensor(global_transform.basis.z * -1.0)
