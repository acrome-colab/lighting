## 
## This base script contains all the components that all robots required to have
## 
extends RigidBody

var vehicle_cmd_msg_js
export var NODE_TYPE: String = 'rider_robot_scoutrider'

func _ready():
	# Subscribe to "vehicle_cmd_msg_js_change" signal so GameManager can notify the robot
	# that vehicle_cmd_msg_js is updated eachtime by javascript
	GameManager.connect("vehicle_cmd_msg_js_change", self, "on_vehicle_cmd_msg_js_change")
	# Get robot message from javascript
	var robot_msg_js = CommunicationManager.get_js_msg(get_path())
	# Defined in vehicle_cmd_msg.js
	vehicle_cmd_msg_js = robot_msg_js.vehicle_cmd
	
func on_vehicle_cmd_msg_js_change():
	## OVERWRITE THIS METHOD TO BE ABLE TO USE IT
	pass

func get_node_type():
	# Will be used for comunication with js purposes.
	return NODE_TYPE
