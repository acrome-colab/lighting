extends Node2D

var bar_red = preload("res://assets/HealthBar/barHorizontal_red.png")
var bar_green = preload("res://assets/HealthBar/barHorizontal_green.png")
var bar_yellow = preload("res://assets/HealthBar/barHorizontal_yellow.png")

onready var healthbar = $HealthBar
onready var label = $HealthBar/HBoxContainer/Label

var min_value = 0
var max_value = 1.2

func _ready():
	pass
	
func _process(delta):
	global_rotation = 0
	
func update_healthbar(value, letter):
	print(max_value)
	healthbar.texture_progress = bar_green
	if value < max_value * 0.7:
		healthbar.texture_progress = bar_yellow
	if value < max_value * 0.35:
		healthbar.texture_progress = bar_red
	healthbar.value = value
	label.text =  letter + ": " + str(stepify(value, 0.01))

func set_range(min_sensor_value,max_sensor_value):
	healthbar.max_value = max_sensor_value
	healthbar.min_value = min_sensor_value
	max_value = max_sensor_value
