extends Sprite3D

onready var bar = $Viewport/HealthBar2D
onready var label = $Label3D
# Called when the node enters the scene tree for the first time.
func _ready():
	texture = $Viewport.get_texture()

func update(value, letter):
	bar.update_healthbar(value, letter)
	if value < GameManager.min_sensor_value or value > GameManager.max_sensor_value:
		label.modulate = Color(255,0,0,255)
	else:
		label.modulate = Color(0,0,205,255)
	label.text = letter + ": " + str(stepify(value, 0.01))

func set_range(min_sensor_value,max_sensor_value):
	bar.set_range(min_sensor_value,max_sensor_value)
