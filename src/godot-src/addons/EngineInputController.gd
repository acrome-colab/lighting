extends Node

var accept_input: bool = true

var robot: Node

func _physics_process(delta):
	if GameConstant.is_web_active and !OS.is_debug_build():
		return
	_game_controller()
	_camera_controller()
	_robot_controller()

func _game_controller():
	if Input.is_action_just_released("ui_accept"):
		GameManager.start()
#	elif Input.is_action_just_released("ui_cancel"):
#		GameManager.restart()

func _camera_controller():
	if Input.is_action_just_released("ui_focus_next"):
		UIManager.change_camera()
	elif Input.is_action_just_released("ui_focus_prev"):
		UIManager.pervious_camera()

func _robot_controller():
	if robot == null:
		return
	var pressed: bool = false
	
	# Sets where to move.
	if Input.is_action_just_released('ui_up'):
		robot.vehicle_cmd_msg_js.move = 1.0
		pressed = true
	elif Input.is_action_just_released("ui_down"):
		robot.vehicle_cmd_msg_js.move = -1.0
		pressed = true
	
	# Sets turning direction
	if Input.is_action_just_released("ui_right"):
		robot.vehicle_cmd_msg_js.turn = -3.14
		pressed = true
	elif Input.is_action_just_released("ui_left"):
		robot.vehicle_cmd_msg_js.turn = 3.14
		pressed = true
	
	if pressed:
		robot.on_vehicle_cmd_msg_js_change()
