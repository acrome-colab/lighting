var path = require('path');

module.exports = {
    entry: './typescript-src/src/startup.ts',
    devtool: 'inline-source-map',
    
    /** Modes **/
    mode: 'production',
    // mode: 'development',
    /** ----- **/

    resolve: {
        extensions: ['.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            configFile: 'typescript-src/tsconfig.json'
                        }
                    }
                ]
            }
        ]
    },
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, '../godot-web-export/'),
    },
};
