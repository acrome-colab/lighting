import os

from fastapi import FastAPI, WebSocket, Request
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
import requests

from typing import List
from pydantic import BaseModel

class File(BaseModel):
    name: str
    content: str
    developerMode:bool

# Starts Api Server
app = FastAPI()

allowed_origin = os.getenv("GODOT_PYTHON_URI") if os.getenv("GODOT_PYTHON_URI") else "127.0.0.1:8000"
user_code_uri = os.getenv("USER_CODE_URI") if os.getenv("USER_CODE_URI") else "../user-code"
project_id = os.getenv("RIDERS_PROJECT_ID") if os.getenv("RIDERS_PROJECT_ID") else 67008
auth_token = os.environ.get("RIDERS_AUTH_TOKEN") if os.environ.get("RIDERS_AUTH_TOKEN") else "3eecfec24668d9d856da43e8c9a7d53c5fa5ed29"
host = os.environ.get("RIDERS_HOST") if os.environ.get("RIDERS_HOST") else "https://api.riders.ai"
scenario_path = "../src/godot-src/ScenarioFiles/"
riders_scenario_path = user_code_uri + "scenario_files/"


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*, 127.0.0.1"], #[allowed_origin],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
    )

# Keeps whether if the client is already connected to a websocket. We will only have one client instance running at any given time.
# The type is List to be able to pass it by reference. So client_manager() can change its value.
is_client_manager_running: List[bool] = [False]

# def send_file_to_api(file_name, developer):
#     path_to_artifact = scenario_path+file_name
#     relative_path = file_name
#     endpoint = "api/v1/project/%s/documents/" % project_id
#     url = "%s/%s" % (host, endpoint)

#     if developer:
#         auth_token = os.environ.get("RIDERS_AUTH_TOKEN") if os.environ.get("RIDERS_AUTH_TOKEN") else "3eecfec24668d9d856da43e8c9a7d53c5fa5ed29"
#     else:
#         auth_token = "3eecfec24668d9d856da43e8c9a7d53c5fa5ed29"

#     response = requests.post(url, 
#         files={'content':open(path_to_artifact,'rb')},
#         data={'relative_path':relative_path},
#         headers={'Authorization': 'Token %s' % auth_token}
#         )
#     print(vars(response))

def game_results_from_stats(game_stats):
    result = dict(**game_stats)
    for key_to_remove in ['robot_scores', 'score_to_win']:
        result.pop(key_to_remove, None)

    for robot_name, robot_points in game_stats['robot_scores'].items():
        result['points_' + robot_name] = robot_points

    result['points_to_win'] = game_stats['score_to_win']
    result['disqualified'] = result['winner'] != 'RexRobot' or not game_stats['is_valid'] or not game_stats['is_finished']
    result['record_disqualified_matches'] = True

    return result

@app.post("/finished-game-stats/")
async def finished_game_stats(finished_game_stats: Request):
    """
    Receives the finished game stats from the client.
    """
    game_stats = await finished_game_stats.json()
    print(game_stats)

    return {"status": "SUCCESS"}

@app.get("/user-code/{file_name}")
async def user_code(file_name):
    """
    Serves the user code to the client.
    """
    with open(f"{user_code_uri}/{file_name}", "r") as f:
        file = f.read()
        return file

@app.get("/get-map-file/{file_name}")
async def get_map(file_name):
    print(project_id)
    print("get method worked")
    arr = []
    with open(f"{riders_scenario_path}{file_name}.gd","r") as f:
        #file = f.read()
        for line in f:
            line = line[:-1] # removed \n
            if "MapSize" in line:
                name, size = line.split(None,1)
                arr.append((name,size))
            elif "Lamp" in line:
                name, x, y, deg = line.split(None,3)
                arr.append((name,x,y,deg))
            else:
                name, x, y = line.split(None,2)
                arr.append((name,x,y))
    return {"items":arr}

@app.post("/send-map-file/")
async def send_map(file: File):
    print(project_id)
    print("send_method worked")
    file_name = file.name
    content = file.content#.split('\n')
    # if file send by developer, it'll storedin ../src/godot-src/ScenarioFiles/
    # if send by user it'll store in riders
    if file.developerMode:
        if os.path.exists(f"{riders_scenario_path}{file_name}.gd"):
            #send_file_to_api(f"{file_name}.gd",file.developerMode)
            return {"status":"FILE EXISTS"}
        else:
            #send_file_to_api(f"{scenario_path}{file_name}.gd")
            with open(f"{riders_scenario_path}{file_name}.gd","a") as f:
                print(content)
                #f.write(content)
                f.close()
                    
    return {"status":"SUCCESS"} 


# Serves static files contained in export folder to client.

# app.mount("/user-code", StaticFiles(directory= user_code_uri), name="user-code-file-server")
app.mount("/", StaticFiles(directory= "../godot-web-export"), name="godot-web-file-server")


