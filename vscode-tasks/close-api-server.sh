kill -9 $(ps -ef | grep "uvicorn main:app" | tr -s ' ' | cut -d ' ' -f4 | head -1)
echo "API server closed"