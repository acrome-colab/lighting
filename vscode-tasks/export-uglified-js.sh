export JS_SRC_PATH="src/javascript-src"
export JS_DEST_PATH="godot-web-export/javascript"
export EXP_OPTIONS="-m eval"

echo "exporting main..."
uglifyjs $JS_SRC_PATH/main.js $EXP_OPTIONS -o $JS_DEST_PATH/main.js

echo "exporting msg_manager..."
uglifyjs $JS_SRC_PATH/msg_manager.js $EXP_OPTIONS -o $JS_DEST_PATH/msg_manager.js

echo "exporting godot..."
uglifyjs $JS_SRC_PATH/godot/*.js $EXP_OPTIONS -o $JS_DEST_PATH/godot.js

echo "exporting msg_definitions..."
uglifyjs $JS_SRC_PATH/msg_definitions/*.js $EXP_OPTIONS -o $JS_DEST_PATH/msg_definitions.js

echo "exporting python..."
uglifyjs $JS_SRC_PATH/python/*.js $EXP_OPTIONS -o $JS_DEST_PATH/python.js

echo "exporting ui..."
uglifyjs $JS_SRC_PATH/ui/*.js $EXP_OPTIONS -o $JS_DEST_PATH/ui.js

echo "Exporting finished."
