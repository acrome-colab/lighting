export CURRENT_DICT=`pwd`

export PYTHON_SRC_PATH="$CURRENT_DICT/src/python-src"
export USER_CODE_PYTHON_SRC_PATH="$CURRENT_DICT/src/ai-robot-python-src"
export PYTHON_DEST_PATH="$CURRENT_DICT/godot-web-export/python"

cd $PYTHON_SRC_PATH 
python3 setup.py bdist_wheel --dist-dir $PYTHON_DEST_PATH
rm -r *.egg-info build

cd $USER_CODE_PYTHON_SRC_PATH
python3 setup.py bdist_wheel --dist-dir $PYTHON_DEST_PATH
rm -r *.egg-info build
