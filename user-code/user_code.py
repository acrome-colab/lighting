#!/usr/bin/env python
import math
from src.rider_lib.rider_robot import RiderRobot
from src.rider_lib.Vector3 import Vector3

class UserCode:
  def __init__(self, robot: RiderRobot):
    self.robot: RiderRobot = robot
    self.wait = False
    self.wait_counter = 0
    self.wait_time = 0.0

  def update_wait(self):
    if self.wait == True:
      self.wait_function(self.wait_time)

  def wait_request(self,wait_time):
    if self.wait == False:
      self.wait = True
      self.wait_time = wait_time

  def wait_function(self,wait_time):
    if self.wait_counter == 0:
      self.wait = True

    if self.wait_counter < wait_time:
      self.wait = True
      self.wait_counter += 1
    else:
      self.wait = False
      self.wait_counter = 0

  def get_distance(self):
    delta = self.robot.opponent_position - self.robot.position
    distance =  math.sqrt(delta.x * delta.x + delta.y * delta.y)
    return distance

  def get_angle(self):
    current_angle = self.robot.rotation
    delta = self.robot.opponent_position - self.robot.position
    target_angle = math.atan2(delta.y, delta.x) # get angle to opponent

    delta_angle = target_angle - current_angle
    if delta_angle > math.pi:
      delta_angle -= math.pi * 2
    if delta_angle < -math.pi:
      delta_angle += math.pi * 2
    return delta_angle

  once = True
  def process_frame(self):
    if self.once:
      return
    # self.robot.move_forward()
    # self.robot.move_backward()
    self.robot.turn_right()
    # self.robot.turn_left()
    self.once = True
