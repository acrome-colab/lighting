# About

This feature branch is made to demonstrate the capabilities of python and godot all working in user's browser.

# Running Requirements

To run: 
Since this demo is running entirely on browser, the only step to run the simulation is to serve the files to the browser. The files are served using fastapi.

To install fastapi:

```shell
pip3 install "fastapi[all]"
```

TODO: add minimum required browser and python versions
TODO: add to develop part

# Running The Code

- In terminal navigate to the project/api folder. Than type:
```shell
python3 -m uvicorn main:app --reload
```
- In browser navigate to 127.0.0.1:8000/index.html
- Wait for game to appear (load)
- Wait until Ready text is observed at the top of page.
- Click 'Run User Code' button to run the code visible on the right.
- You should observe the car start moving.

- If you want, you can change 'user_code.py' from user-code/ folder and click 'Run User Code' button again to see the change real-time in simulation.

# Developing Requirements

To develop, you only need Godot 3.4 Standard version and other requirements from "Running Requirements" part.
Note: You might also find it useful to check Godot 3.4's own requirements.

- Standard Version of godot: https://godotengine.org/download/
    Note: For macOS users, it is recommended to install Godot using 'brew'. You can find the instructions at the bottom of the link above.

- After Installing Godot, you need to download the export templates provided by Godot to be able to export your Godot project.
    To install export templates: Navigate to godot/ folder located in this project, then double click project.godot to open Godot Editor.
    After the editor opened, from the top navigation menu, click to: Editor -> Manage Export Templates... This will open a new window.
    From the window, click "Download and Install" button to install required templates.

- Optional: It is recommended for you to add Godot to path. So that you can execute Godot commands from you command line to make your development more efficient.
    For more information: https://docs.godotengine.org/en/stable/tutorials/editor/command_line_tutorial.html

# The Structure
TODO: (Ali) Expand here...

The project 4 main components:
- Godot code
- Python code
- JavaScript Code
- Api code
- user_code code

# Make Changes in Godot Side

For any change you make in src/godot-src/ folder, you need to export Godot. To do that, you have 2 options:

- Using command line: 
    - From terminal, navigate to godot folder: cd src/godot-src/
    - Then type: godot --export "HTML5" --no-window

- Manually from the editor:
    - Open project from editor by double clicking project.godot file located in src/godot-src/ folder.
    - From the top navigation menu, click: Project -> Export.. -> HTML5 -> Export Project

Note: The first option requires Godot to be in your path. More information is in "Developing Requirements" section.
Note2: After you exported godot, if the Fastapi server is already running, you don't need to restart it. 

# Make Changes in Python Side

Just like Godot, for any changes you make in src/python-src/ folder, you need to build the python wheel. To do that:

- From terminal navigate to src/python-src/
- Then type: python3 setup.py bdist_wheel --dist-dir ../../godot-web-export/python

Note: After you build the wheel, if the Fastapi server is already running, you don't need to restart it. 

# Make Changes in Javascript/HTML Side

- If you need to change "index.html" located in godot-web-export/ folder:
    - Go to src/godot-src/web/ and change "web_export_template.html" from there. 
    - Then export godot project.

- To change javascript codes, you need to edit js files in src/javascript-src/ folder:
    - After editing sources, you should uglify and minify them. There is an uglify-js module in npm. To install:
        ```shell
        npm install uglify-js -g
        ```
    - In Visual Studio Code, we have uglify-js task to handle the processes with specified options. Task moves output files to godot-web-export/javascript/ folder after uglifying steps.
